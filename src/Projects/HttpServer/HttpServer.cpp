/*
    MYCP is a HTTP and C++ Web Application Server.
    Copyright (C) 2009-2010  Akee Yang <akee.yang@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define USES_OPENSSL
#ifdef USES_OPENSSL
#ifdef WIN32
#pragma comment(lib, "libeay32.lib")  
#pragma comment(lib, "ssleay32.lib") 
#endif // WIN32
#endif // USES_OPENSSL
#include "CgcTcpClient.h"
#ifdef WIN32
#pragma warning(disable:4267 4996)
//#include <windows.h>
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
    return TRUE;
}
#endif

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

// cgc head
#include <CGCBase/http.h>
#include <CGCBase/cgcServices.h>
#include <CGCBase/cgcCDBCService.h>
#include <CGCBase/cgcutils.h>
//#include <CGCClass/cgcclassinclude.h>
using namespace cgc;
#include "MycpHttpServer.h"
#include "XmlParseHosts.h"
#include "XmlParseApps.h"
#include "XmlParseDSs.h"
#include "md5.h"
//#include <ThirdParty/fastcgi/fastcgi.h>
#include "fastcgi.h"
#include "cgcaddress.h"

//#define USES_BODB_SCRIPT
#ifdef WIN32
#include "shellapi.h"
#pragma comment(lib, "shell32.lib")  
#include "tlhelp32.h"
#include "Psapi.h"
#pragma comment(lib, "Psapi.lib")
void KillAllProcess(const char* lpszProcessName)
{
	HANDLE hProcessSnap = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);  
	if (hProcessSnap==NULL) return;
	PROCESSENTRY32 pe32;  
	memset(&pe32,0,sizeof(pe32));
	pe32.dwSize=sizeof(PROCESSENTRY32);  
	if (::Process32First(hProcessSnap,&pe32))  
	{  
		do  
		{
			const std::string sExeFile(pe32.szExeFile);
			std::string::size_type find = sExeFile.find(lpszProcessName);
			if (find != std::string::npos)
			{
				HANDLE hProcess = OpenProcess(SYNCHRONIZE|PROCESS_TERMINATE, FALSE, pe32.th32ProcessID);
				if(NULL != hProcess)
				{
					TerminateProcess(hProcess, 0);
				}
			}
		}while(::Process32Next(hProcessSnap,&pe32));   
	}
	CloseHandle(hProcessSnap);

	HWND hHwnd = FindWindow(NULL,lpszProcessName);
	if (hHwnd!=NULL)
	{
		DWORD dwWndProcessId = 0;
		GetWindowThreadProcessId(hHwnd,&dwWndProcessId);
		if (dwWndProcessId>0)
		{
			HANDLE hProcess = OpenProcess(SYNCHRONIZE|PROCESS_TERMINATE, FALSE, dwWndProcessId);
			if(NULL != hProcess)
			{
				TerminateProcess(hProcess, 0);
			}
		}
	}
}
#endif

#ifdef USES_BODB_SCRIPT
cgcCDBCService::pointer theCdbcService;
#endif
//std::string theFastcgiPHPServer;
CFastcgiInfo::pointer thePHPFastcgiInfo;
bool theEnableDataSource = false;
cgcServiceInterface::pointer theFileSystemService;
//cgcServiceInterface::pointer theStringService;
XmlParseHosts theVirtualHosts;
XmlParseApps::pointer theApps;
XmlParseDSs::pointer theDataSources;
CVirtualHost::pointer theDefaultHost;
CLockMap<tstring, CFileScripts::pointer> theFileScripts;
CLockMap<tstring, CCSPFileInfo::pointer> theFileInfos;
CLockMap<tstring, CCSPFileInfo::pointer> theIdleFileInfos;	// 记录空闲文件，超过7天无访问，删除数据库数据；
cgcAttributes::pointer theAppAttributes;
const int ATTRIBUTE_FILE_INFO = 8;				// filepath->
const unsigned int TIMERID_1_SECOND = 1;		// 1秒检查一次

class CResInfo : public cgcObject
{
public:
	typedef boost::shared_ptr<CResInfo> pointer;
	static CResInfo::pointer create(const std::string& sFileName, const std::string& sMimeType)
	{
		return CResInfo::pointer(new CResInfo(sFileName,sMimeType));
	}
	CResInfo(const std::string& sFileName, const std::string& sMimeType)
		: m_sFileName(sFileName),m_sMimeType(sMimeType)
		, m_tModifyTime(0),m_tRequestTime(0)
		, m_nSize(0),m_pData(NULL)
	{
	}
	CResInfo(void)
		: m_tModifyTime(0),m_tRequestTime(0)
		, m_nSize(0),m_pData(NULL)
	{
	}
	virtual ~CResInfo(void)
	{
		if (m_pData!=NULL)
		{
			delete[] m_pData;
			m_pData = NULL;
		}
		//m_fs.close();
	}
	boost::mutex m_mutex;
	std::string m_sFileName;
	std::string m_sMimeType;
	//std::fstream m_fs;
	time_t m_tModifyTime;
	std::string m_sModifyTime;
	std::string m_sETag;
	time_t m_tRequestTime;
	unsigned int m_nSize;
	char * m_pData;
};

typedef enum REQUEST_INFO_STATE
{
	REQUEST_INFO_STATE_WAITTING_RESPONSE
	, REQUEST_INFO_STATE_FCGI_DOING
	, REQUEST_INFO_STATE_FCGI_STDOUT
	, REQUEST_INFO_STATE_FCGI_STDERR
	, REQUEST_INFO_STATE_FCGI_DISCONNECTED
};
#define DEFAULT_SEND_BUFFER_SIZE (32*1024)

class CRequestPassInfo
{
public:
	typedef boost::shared_ptr<CRequestPassInfo> pointer;
	static CRequestPassInfo::pointer create(int nRequestId, const std::string& sFastcgiPass)
	{
		return CRequestPassInfo::pointer(new CRequestPassInfo(nRequestId, sFastcgiPass));
	}
	int GetRequestId(void) const {return m_nRequestId;}
	const std::string& GetFastcgiPass(void) const {return m_sFastcgiPass;}
	void SetProcessId(unsigned int v) {m_nProceessId = v;}
	unsigned int GetProcessId(void) const {return m_nProceessId;}
	void SetProcessHandle(void* v) {m_pProcessHandle = v;}
	void* GetProcessHandle(void) const {return m_pProcessHandle;}
	int GetRequestCount(void) const  {return m_nRequestCount;}
	int IncreaseRequestCount(void) {return ++m_nRequestCount;}
	void ResetRequestCount(void) {m_nRequestCount=0;}

	void KillProcess(void)
	{
		if (m_nProceessId>0)
		{
#ifdef WIN32
			HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, FALSE, m_nProceessId);
			if (hProcess!=NULL)
			{
				TerminateProcess(hProcess, 0);
			}else if (m_pProcessHandle!=NULL)
			{
				TerminateProcess((HANDLE)m_pProcessHandle, 0);
			}
#else
			if (!m_sFastcgiPass.empty())
			{
				// ps -ef|grep '127.0.0.1:9000' | awk '{print $2}' | xargs kill
				char lpszBuffer[256];
				sprintf(lpszBuffer,"ps -ef|grep -v 'grep'|grep '%s' | awk '{print $2}' | xargs kill",m_sFastcgiPass.c_str());
				//sprintf(lpszBuffer,"ps -ef|grep '%s' | awk '{print $2}' | xargs kill",m_sFastcgiPass.c_str());
				system(lpszBuffer);
			}
#endif
			m_nProceessId = 0;
			m_pProcessHandle = NULL;
			m_nRequestCount = 0;
		}
	}

	CRequestPassInfo(int nRequestId, const std::string& sFastcgiPass)
		: m_nRequestId(nRequestId), m_sFastcgiPass(sFastcgiPass)
		, m_nProceessId(0), m_pProcessHandle(NULL)
		, m_nRequestCount(0)
	{}
	CRequestPassInfo(void)
		: m_nRequestId(0)
		, m_nProceessId(0), m_pProcessHandle(NULL)
		, m_nRequestCount(0)
	{}
	virtual ~CRequestPassInfo(void)
	{
		KillProcess();
	}
private:
	int m_nRequestId;
	std::string m_sFastcgiPass;
	unsigned int m_nProceessId;
	void* m_pProcessHandle;
	int m_nRequestCount;
};
typedef enum SCRIPT_FILE_TYPE
{
	SCRIPT_FILE_TYPE_UNKNOWN
	, SCRIPT_FILE_TYPE_CSP
	, SCRIPT_FILE_TYPE_PHP
};

class CFastcgiRequestInfo
{
public:
	typedef boost::shared_ptr<CFastcgiRequestInfo> pointer;
	static CFastcgiRequestInfo::pointer create(const CRequestPassInfo::pointer& pRequestPassInfo, const cgcHttpResponse::pointer& response)
	{
		return CFastcgiRequestInfo::pointer(new CFastcgiRequestInfo(pRequestPassInfo, response));
	}
	
	cgc::CgcTcpClient::pointer m_pFastCgiServer;

	const CRequestPassInfo::pointer& GetRequestPassInfo(void) const {return m_pRequestPassInfo;}
	int GetRequestId(void) const {return m_pRequestPassInfo->GetRequestId();}
	const std::string& GetFastcgiPass(void) const {return m_pRequestPassInfo->GetFastcgiPass();}
	const cgcHttpResponse::pointer& response(void) const {return m_response;}
	time_t GetRequestTime(void) const {return m_tRequestTime;}
	void SetResponseState(REQUEST_INFO_STATE nResponseState) {m_nResponseState = nResponseState;}
	REQUEST_INFO_STATE GetResponseState(void) const {return m_nResponseState;}
	void SetResponsePaddingLength(int nPaddingLength) {m_nResponsePaddingLength = nPaddingLength;}
	int GetResponsePaddingLength(void) const {return m_nResponsePaddingLength;}
	void SetLastPaddingData(const std::string& sData) {m_sLastPaddingData = sData;}
	const std::string GetLastPaddingData(void) const {return m_sLastPaddingData;}

	unsigned char* GetBuffer(void) {return m_lpszBuffer;}

	CFastcgiRequestInfo(const CRequestPassInfo::pointer& pRequestPassInfo, const cgcHttpResponse::pointer& response)
		: m_pRequestPassInfo(pRequestPassInfo), m_response(response), m_nResponseState(REQUEST_INFO_STATE_WAITTING_RESPONSE)
		, m_nResponsePaddingLength(0)
	//CFastcgiRequestInfo(int nRequestId, const std::string& sFastcgiPass, const cgcHttpResponse::pointer& response)
	//	: m_nRequestId(nRequestId), m_sFastcgiPass(sFastcgiPass), m_response(response), m_nResponseState(REQUEST_INFO_STATE_WAITTING_RESPONSE)
	//	, m_nResponsePaddingLength(0)
	{
		m_tRequestTime = time(0);
		m_lpszBuffer = new unsigned char[DEFAULT_SEND_BUFFER_SIZE+1];
	}
	virtual ~CFastcgiRequestInfo(void)
	{
		m_pFastCgiServer.reset();
		delete[] m_lpszBuffer;
	}
private:
	CRequestPassInfo::pointer m_pRequestPassInfo;
	//int m_nRequestId;
	//std::string m_sFastcgiPass;
	cgcHttpResponse::pointer m_response;
	time_t m_tRequestTime;
	REQUEST_INFO_STATE m_nResponseState;
	int m_nResponsePaddingLength;
	std::string m_sLastPaddingData;
	unsigned char* m_lpszBuffer;
};

class CHttpTimeHandler
	: public cgcOnTimerHandler
	, public cgc::TcpClient_Callback	// for tcp
{
public:
	typedef boost::shared_ptr<CHttpTimeHandler> pointer;
	static CHttpTimeHandler::pointer create(void)
	{
		return CHttpTimeHandler::pointer(new CHttpTimeHandler());
	}
	CHttpTimeHandler(void)
		: m_nDefaultRequestPassIndex(0)
	{
	}
	virtual ~CHttpTimeHandler(void)
	{
		m_pRequestPassInfoList.clear();
		m_pIdlePassInfoList.clear();
		m_pDefaultRequestPassInfo.reset();
	}

#ifndef WIN32
	int FindPidByName(const char* lpszName)
	{
		int nPid = 0;
		char lpszCmd[128];
		sprintf(lpszCmd,"ps -ef|grep -v 'grep'|grep '%s' | awk '{print $2}'",lpszName);
		//sprintf(lpszCmd,"ps -ef|grep '%s' | awk '{print $2}'",lpszName);
		FILE * f = popen(lpszCmd, "r");
		if (f==NULL)
			return -1;
		memset(lpszCmd,0,sizeof(lpszCmd));
		while (fgets(lpszCmd,128,f)!=NULL)
		{
			const int ret = atoi(lpszCmd);
			if (ret>0)
			{
				pclose(f);
				nPid = ret;
				//printf("**** 11 pid=%d,%s\n",nPid,lpszCmd);
				return nPid;
			}
		}
		pclose(f);
		nPid = atoi(lpszCmd);
		//printf("**** 22 pid=%d,%s\n",nPid,lpszCmd);
		return nPid;
	}
#endif

	CFastcgiRequestInfo::pointer GetFastcgiRequestInfo(SCRIPT_FILE_TYPE nScriptFileType, const cgcHttpResponse::pointer& response)
	{
		CRequestPassInfo::pointer pRequestPassInfo;
		if (!m_pRequestPassInfoList.front(pRequestPassInfo))
		{
			if (thePHPFastcgiInfo->m_nCurrentFcgiPassPortIndex>=thePHPFastcgiInfo->m_nMaxProcess && m_pDefaultRequestPassInfo.get()!=NULL)
			{
				// 超时最大进程数，使用默认等待
				const int nRequestId = thePHPFastcgiInfo->m_nCurrentFcgiPassPortIndex+(++m_nDefaultRequestPassIndex);
				pRequestPassInfo = m_pDefaultRequestPassInfo;
				CreateRequestPassInfoProcess(pRequestPassInfo);	// 如果已经关闭，启动一次
			}else if (!m_pIdlePassInfoList.front(pRequestPassInfo))
			{
				const int nRequestId = 1+(thePHPFastcgiInfo->m_nCurrentFcgiPassPortIndex++);
				char lpszBuffer[128];
				sprintf(lpszBuffer,"%s:%d",thePHPFastcgiInfo->m_sFcgiPassIp.c_str(),(int)thePHPFastcgiInfo->m_nFcgiPassPort+nRequestId-1);
				pRequestPassInfo = CRequestPassInfo::create(nRequestId,lpszBuffer);
				CreateRequestPassInfoProcess(pRequestPassInfo);
				if (m_pDefaultRequestPassInfo.get()==NULL)
					m_pDefaultRequestPassInfo = pRequestPassInfo;
			}else if (pRequestPassInfo->GetProcessId()==0)
			{
				CreateRequestPassInfoProcess(pRequestPassInfo);
			}
		//}else if (pRequestPassInfo->GetProcessId()==0 && !thePHPFastcgiInfo->m_sFastcgiPath.empty())
		//{
		//	CreateRequestPassInfoProcess(pRequestPassInfo);
		}
		pRequestPassInfo->IncreaseRequestCount();
		CFastcgiRequestInfo::pointer pFastcgiRequestInfo = CFastcgiRequestInfo::create(pRequestPassInfo,response);
		m_pFastcgiRequestList.insert(pRequestPassInfo->GetRequestId(),pFastcgiRequestInfo);
		return pFastcgiRequestInfo;
	}
	//void SetRequestId(int nRequestId)
	//{
	//	m_pRequestPassInfoList.add(nRequestId);
	//}
	bool RemoveRequestInfo(const CFastcgiRequestInfo::pointer& pFastcgiRequestInfo)
	{
		if (m_pFastcgiRequestList.remove(pFastcgiRequestInfo->GetRequestId()))
		{
			m_pRequestPassInfoList.add(pFastcgiRequestInfo->GetRequestPassInfo());
			return true;
		}
		return false;
	}

	void CreateRequestPassInfoProcess(const CRequestPassInfo::pointer& pRequestPassInfo)
	{
		if (pRequestPassInfo.get()==NULL || (pRequestPassInfo->GetProcessId()>0 && pRequestPassInfo->GetProcessHandle()!=NULL)) return;
		char lpszBuffer[512];
#ifdef WIN32
		// php-cgi.exe -b 127.0.0.1:9000 -c php.ini
		if (thePHPFastcgiInfo->m_sFastcgiPath.empty())
			sprintf(lpszBuffer,"php-cgi.exe -b %s -c php.ini",pRequestPassInfo->GetFastcgiPass().c_str());
		else
			sprintf(lpszBuffer,"\"%s/php-cgi.exe\" -b %s -c \"%s/php.ini\"",thePHPFastcgiInfo->m_sFastcgiPath.c_str(),pRequestPassInfo->GetFastcgiPass().c_str(),thePHPFastcgiInfo->m_sFastcgiPath.c_str());
		PROCESS_INFORMATION pi;
		STARTUPINFO si;
		memset(&si,0,sizeof(si));
		si.cb=sizeof(si);
		si.wShowWindow=SW_HIDE;
		si.dwFlags=STARTF_USESHOWWINDOW;
		CreateProcess(NULL,lpszBuffer,NULL,FALSE,FALSE,0,NULL,NULL,&si,&pi);
		pRequestPassInfo->SetProcessHandle(pi.hProcess);
		pRequestPassInfo->SetProcessId(pi.dwProcessId);
		//printf("**** CreateProcess Id=%d,Handle=%d,LastError=%d\n",pi.dwProcessId,(int)pi.hProcess,GetLastError());
#else
		// php-cgi -b 127.0.0.1:9000 -c php.ini
		if (thePHPFastcgiInfo->m_sFastcgiPath.empty())
			sprintf(lpszBuffer,"php-cgi -b %s -c \"/etc/php.ini\" &",pRequestPassInfo->GetFastcgiPass().c_str());
		else
		{
			sprintf(lpszBuffer,"cd \"%s\" ; ./php-cgi -b %s -c php.ini &",thePHPFastcgiInfo->m_sFastcgiPath.c_str(),pRequestPassInfo->GetFastcgiPass().c_str());
			//sprintf(lpszBuffer,"cd \"%s\" && ./php-cgi -b %s -c php.ini &",thePHPFastcgiInfo->m_sFastcgiPath.c_str(),pRequestPassInfo->GetFastcgiPass().c_str());
		}
		system(lpszBuffer);
		for (int i=0;i<100;i++)	// 2S
		{
			usleep(20000);
			const int nPid = FindPidByName(pRequestPassInfo->GetFastcgiPass().c_str());
			if (nPid>0)
			{
				pRequestPassInfo->SetProcessHandle((void*)nPid);
				pRequestPassInfo->SetProcessId(nPid);
				break;
			}
		}
#endif

	}
	void PHP_FCGIPM(void)
	{
		if (thePHPFastcgiInfo.get()==NULL)
			return;

		int nFastcgiProcessCount = 0;
		bool bContinue = true;
		while(bContinue)
		{
			nFastcgiProcessCount = 0;
			bContinue = false;
			BoostWriteLock wtlock(m_pRequestPassInfoList.mutex());
			CLockList<CRequestPassInfo::pointer>::iterator pIter = m_pRequestPassInfoList.begin();
			for (; pIter!=m_pRequestPassInfoList.end(); pIter++)
			{
				CRequestPassInfo::pointer pRequestPassInfo = *pIter;
				if (pRequestPassInfo->GetProcessId()==0 || pRequestPassInfo->GetProcessHandle()==NULL)
				{
					//bContinue = true;
					//m_pRequestPassInfoList.erase(pIter);
					//break;
					continue;
				}
				nFastcgiProcessCount++;

				if (pRequestPassInfo->GetRequestCount()>=thePHPFastcgiInfo->m_nMaxRequestRestart || nFastcgiProcessCount>thePHPFastcgiInfo->m_nMinProcess)
				{
					// 超时最大请求次数，关闭
					// 超过最小进程数量，关闭
					// * 控制一次只退出一个process
					m_pRequestPassInfoList.erase(pIter);
					wtlock.unlock();
					pRequestPassInfo->KillProcess();
					m_pIdlePassInfoList.add(pRequestPassInfo);
					m_nDefaultRequestPassIndex = 0;
					break;
				}
#ifdef WIN32
				DWORD ExitCode = STILL_ACTIVE;
				GetExitCodeProcess((HANDLE)pRequestPassInfo->GetProcessHandle(),&ExitCode);
				//printf("**** GetExitCodeProcess %d -> %d\n",pRequestPassInfo->GetProcessId(),ExitCode);
				if (ExitCode!=STILL_ACTIVE)
				{
					pRequestPassInfo->KillProcess();
					CreateRequestPassInfoProcess(pRequestPassInfo);
				}
#else
				const int nPID = FindPidByName(pRequestPassInfo->GetFastcgiPass().c_str());
				if (nPID<=0)
				{
					pRequestPassInfo->KillProcess();
					CreateRequestPassInfoProcess(pRequestPassInfo);
				}
#endif
			}
		}
	}

	virtual void OnTimeout(unsigned int nIDEvent, const void * pvParam)
	{
		if (nIDEvent==TIMERID_1_SECOND)
		{
			static unsigned int theSecondIndex = 0;
			theSecondIndex++;

			if ((theSecondIndex%10)==9)	// 10秒处理一次
			{
				PHP_FCGIPM();
			}

			if ((theSecondIndex%(24*3600))==23*3600)	// 一天处理一次
			{
				std::vector<tstring> pRemoveFileNameList;
				if (!theIdleFileInfos.empty())
				{
					// 删除超过10天没用文件数据；
					const time_t tNow = time(0);
#ifdef USES_BODB_SCRIPT
					char sql[512];
#endif
					BoostReadLock rdlock(theIdleFileInfos.mutex());
					CLockMap<tstring, CCSPFileInfo::pointer>::iterator pIter = theIdleFileInfos.begin();
					for (; pIter!=theIdleFileInfos.end(); pIter++)
					{
						CCSPFileInfo::pointer fileInfo = pIter->second;
						if (fileInfo->m_tRequestTime==0 || (fileInfo->m_tRequestTime+(10*24*3600))<tNow)	// ** 10 days
						{
							const tstring sFileName(pIter->first);
							pRemoveFileNameList.push_back(sFileName);
							theFileInfos.remove(sFileName);
							theFileScripts.remove(sFileName);
#ifdef USES_BODB_SCRIPT
							if (theCdbcService.get()!=NULL)
							{
								tstring sFileNameTemp(sFileName);
								theCdbcService->escape_string_in(sFileNameTemp);
								sprintf(sql, "DELETE FROM scriptitem_t WHERE filename='%s'", sFileNameTemp.c_str());
								theCdbcService->execute(sql);
								sprintf(sql, "DELETE FROM cspfileinfo_t WHERE filename='%s')",sFileNameTemp.c_str());
								theCdbcService->execute(sql);
							}
#endif
						}
					}
				}
				for (size_t i=0; i<pRemoveFileNameList.size(); i++)
				{
					theIdleFileInfos.remove(pRemoveFileNameList[i]);
				}
				pRemoveFileNameList.clear();

			}
			if ((theSecondIndex%3600)==3500)	// 3600=60分钟处理一次
			{
				const time_t tNow = time(0);

				std::vector<tstring> pRemoveFileNameList;
				if (!theFileInfos.empty())
				{
					// 超过2小时没有访问文件， 放到空闲列表；
					BoostReadLock rdlock(theFileInfos.mutex());
					CLockMap<tstring, CCSPFileInfo::pointer>::iterator pIter = theFileInfos.begin();
					for (; pIter!=theFileInfos.end(); pIter++)
					{
						CCSPFileInfo::pointer fileInfo = pIter->second;
						if (fileInfo->m_tRequestTime>0 && (fileInfo->m_tRequestTime+(3*3600))<tNow)	// ** 3 hours
						{
							if (!theFileScripts.remove(pIter->first))
							{
								pRemoveFileNameList.push_back(pIter->first);
								theIdleFileInfos.insert(pIter->first,pIter->second,false);
							}
						}
					}
				}
				for (size_t i=0; i<pRemoveFileNameList.size(); i++)
				{
					theFileInfos.remove(pRemoveFileNameList[i]);
				}
				pRemoveFileNameList.clear();

				StringObjectMapPointer pFileInfoList = theAppAttributes->getStringAttributes(ATTRIBUTE_FILE_INFO,false);
				if (pFileInfoList.get() != NULL && !pFileInfoList->empty())
				{
					std::vector<tstring> pRemoveFilePathList;
					{
						BoostReadLock rdlock(pFileInfoList->mutex());
						CObjectMap<tstring>::const_iterator pIter = pFileInfoList->begin();
						for (;pIter!=pFileInfoList->end();pIter++)
						{
							const CResInfo::pointer pResInfo = CGC_OBJECT_CAST<CResInfo>(pIter->second);
							if (pResInfo->m_tRequestTime>0 && (pResInfo->m_tRequestTime+(3*3600))<tNow)	// ** 3 hours
							{
								pRemoveFilePathList.push_back(pIter->first);
							}
						}
					}
					for (size_t i=0; i<pRemoveFilePathList.size(); i++)
					{
						pFileInfoList->remove(pRemoveFilePathList[i]);
						//theAppAttributes->removeAttribute(ATTRIBUTE_FILE_INFO, pRemoveFilePathList[i]);
					}
					pRemoveFilePathList.clear();
				}
			}

		}
	}
	virtual void OnDisconnect(int nUserData)
	{
		printf("******** OnDisconnect UserData=%d\n",nUserData);
		const int nTcpRequestId = nUserData;
		CFastcgiRequestInfo::pointer pFastcgiRequestInfo;
		if (!m_pFastcgiRequestList.find(nTcpRequestId,pFastcgiRequestInfo,true))
		{
			return;
		}
		for (int i=0;i<100;i++)
		{
			if (pFastcgiRequestInfo->GetResponseState()!=REQUEST_INFO_STATE_FCGI_DOING)
				break;
#ifdef WIN32
			Sleep(10);
#else
			usleep(10000);
#endif
		}
		pFastcgiRequestInfo->SetResponseState(REQUEST_INFO_STATE_FCGI_DISCONNECTED);
		m_pRequestPassInfoList.add(pFastcgiRequestInfo->GetRequestPassInfo());

	}
	virtual void OnReceiveData(const ReceiveBuffer::pointer& data, int nUserData)
	{
		printf("******** OnReceiveData size=%d,UserData=%d\n",data->size(),nUserData);
		const int nTcpRequestId = nUserData;
		CFastcgiRequestInfo::pointer pFastcgiRequestInfo;
		if (!m_pFastcgiRequestList.find(nTcpRequestId,pFastcgiRequestInfo))
		{
			return;
		}

		if (pFastcgiRequestInfo->GetResponseState()==REQUEST_INFO_STATE_WAITTING_RESPONSE && data->size()>=FCGI_HEADER_LEN)
		{
			FCGI_Header header;
			memcpy(&header,data->data(),8);

			if(header.version != FCGI_VERSION_1) {
				return ;//FCGX_UNSUPPORTED_VERSION;
			}
			const int nRequestId = (header.requestIdB1 << 8) + header.requestIdB0;
			if (nRequestId!=nTcpRequestId)
				return;
			const int nContentLen = (header.contentLengthB1 << 8)	+ header.contentLengthB0;
			const int nPaddingLen = header.paddingLength;
			printf("******** RequestId=%d,type=%d,ContentLen=%d,PaddingLen=%d\n",nRequestId,(int)header.type,nContentLen,nPaddingLen);
			pFastcgiRequestInfo->SetResponsePaddingLength(nPaddingLen);

			try
			{
				if (header.type == FCGI_STDOUT)
				{
					pFastcgiRequestInfo->SetResponseState(REQUEST_INFO_STATE_FCGI_DOING);
					//printf("********\n%s\n", data->data()+8);
					//const char * findSearch = NULL;
					const char * findSearchEnd = NULL;
					const char * httpRequest = (const char*)(data->data()+FCGI_HEADER_LEN);
					int nBodyLength = nContentLen;
					bool bFindHttpHead = false;
					while (nBodyLength>0 && httpRequest != NULL)
					{
						findSearchEnd = strstr(httpRequest, "\r\n");
						if (findSearchEnd==NULL)
						{
							//printf("******** 1 length=%d,data=%s\n",nBodyLength,httpRequest);
							if (nBodyLength>0)
								pFastcgiRequestInfo->response()->writeData(httpRequest,nBodyLength);
							break;
						}else if (findSearchEnd==httpRequest)
						{
							if (bFindHttpHead)
							{
								httpRequest = httpRequest+2;
								nBodyLength -= 2;
							}else
							{
								httpRequest = httpRequest+4;
								nBodyLength -= 4;
							}
							//printf("******** 2 length=%d,data=%s\n",nBodyLength,httpRequest);
							if (nBodyLength>0)
								pFastcgiRequestInfo->response()->writeData(httpRequest,nBodyLength);
							break;
						}
						const std::string sLine(httpRequest,findSearchEnd-httpRequest);
						const std::string::size_type find = sLine.find(":");
						if (find==std::string::npos)
						{
							break;
						}
						const tstring sParamReal(sLine.substr(0,find));
						tstring param(sParamReal);
						std::transform(param.begin(), param.end(), param.begin(), ::tolower);
						const short nOffset = sLine.c_str()[find+1]==' '?2:1;	// 带空格2，不带空格1
						const tstring value(sLine.substr(find+nOffset));

						//findSearch = strstr(httpRequest, ":");
						//if (findSearch == NULL)
						//{
						//	if (nBodyLength>=2 && httpRequest[0]=='\r' && httpRequest[1]=='\n')
						//	{
						//		httpRequest = httpRequest+2;
						//		nBodyLength -= 2;
						//	}
						//	printf("******** data=%s,length=%d\n",httpRequest,nBodyLength);
						//	pFastcgiRequestInfo->response()->writeData(httpRequest,nBodyLength);
						//	break;
						//}
						//findSearchEnd = strstr(findSearch+1, "\r\n");
						//if (findSearchEnd == NULL) break;
						//const tstring sParamReal(httpRequest, findSearch-httpRequest);
						//tstring param(sParamReal);
						//std::transform(param.begin(), param.end(), param.begin(), ::tolower);
						//const short nOffset = findSearch[1]==' '?2:1;	// 带空格2，不带空格1
						//tstring value(findSearch+nOffset, findSearchEnd-findSearch-nOffset);

						bFindHttpHead = true;
						//printf("******** PV-> %s:%s\n",sParamReal.c_str(),value.c_str());
						if (param=="content-type")
						{
							pFastcgiRequestInfo->response()->setContentType(value);
							//}else if (param=="content-length")
							//{
							//	//pFastcgiRequestInfo->response()->setcon(value);
						}else if (param=="status")
						{
							const HTTP_STATUSCODE nHttpState = (HTTP_STATUSCODE)atoi(value.c_str());
							pFastcgiRequestInfo->response()->setStatusCode(nHttpState);
						}else
						{
							pFastcgiRequestInfo->response()->setHeader(sParamReal,value);
						}
						nBodyLength -= (int)(findSearchEnd-httpRequest);
						nBodyLength -= 2;	// \r\n
						httpRequest = findSearchEnd+2;
					}
					pFastcgiRequestInfo->SetResponseState(REQUEST_INFO_STATE_FCGI_STDOUT);
				}else if (header.type == FCGI_STDERR)
				{
					//printf("********\n%s\n", data->data()+8);
					pFastcgiRequestInfo->response()->writeData((const char*)(data->data()+FCGI_HEADER_LEN),nContentLen);
					pFastcgiRequestInfo->SetResponseState(REQUEST_INFO_STATE_FCGI_STDERR);
				//}else if (header.type == FCGI_END_REQUEST)
				//{
				}
			}catch(std::exception const &)
			{
			}catch(...)
			{}
			//pFastcgiRequestInfo->SetResponsed();
			//m_pRequestPassInfoList.add(nRequestId);
		}else if (pFastcgiRequestInfo->GetResponseState()>=REQUEST_INFO_STATE_FCGI_DOING && data->size()>pFastcgiRequestInfo->GetResponsePaddingLength())
		{
			//FCGI_Header header;
			//memcpy(&header,data->data(),8);
			//if(header.version == FCGI_VERSION_1)
			//{
			//	const int nRequestId = (header.requestIdB1 << 8) + header.requestIdB0;
			//	if (nRequestId==nTcpRequestId)
			//	{
			//		const int nContentLen = (header.contentLengthB1 << 8)	+ header.contentLengthB0;
			//		const int nPaddingLen = header.paddingLength;
			//		printf("******** 22 RequestId=%d,type=%d,ContentLen=%d,PaddingLen=%d\n",nRequestId,(int)header.type,nContentLen,nPaddingLen);
			//	}
			//}
			try
			{
				if (pFastcgiRequestInfo->GetResponsePaddingLength()>0)
				{
					const std::string& sLastPaddingData = pFastcgiRequestInfo->GetLastPaddingData();
					if (!sLastPaddingData.empty())
						pFastcgiRequestInfo->response()->write(sLastPaddingData);
					const std::string sPaddingData((const char*)data->data()+(data->size()-pFastcgiRequestInfo->GetResponsePaddingLength()),pFastcgiRequestInfo->GetResponsePaddingLength());
					pFastcgiRequestInfo->SetLastPaddingData(sPaddingData);
					//printf("****paddingdata\n%s\n",sPaddingData.c_str());
				}
				pFastcgiRequestInfo->response()->writeData((const char*)data->data(),data->size()-pFastcgiRequestInfo->GetResponsePaddingLength());
				//pFastcgiRequestInfo->response()->writeData((const char*)data->data(),data->size());
			}catch(std::exception const &)
			{
			}catch(...)
			{}
		}
	}
private:
	CLockList<CRequestPassInfo::pointer> m_pRequestPassInfoList;
	CRequestPassInfo::pointer m_pDefaultRequestPassInfo;
	int m_nDefaultRequestPassIndex;
	CLockList<CRequestPassInfo::pointer> m_pIdlePassInfoList;
	CLockMap<int,CFastcgiRequestInfo::pointer> m_pFastcgiRequestList;
};
CHttpTimeHandler::pointer theTimerHandler;
//cgc::CgcTcpClient::pointer m_pFastCgiServer;

static FCGI_Header MakeHeader(
        int type,
        int requestId,
        int contentLength,
        int paddingLength)
{
    FCGI_Header header;
    //ASSERT(contentLength >= 0 && contentLength <= FCGI_MAX_LENGTH);
    //ASSERT(paddingLength >= 0 && paddingLength <= 0xff);
    header.version = FCGI_VERSION_1;
    header.type             = (unsigned char) type;
    header.requestIdB1      = (unsigned char) ((requestId     >> 8) & 0xff);
    header.requestIdB0      = (unsigned char) ((requestId         ) & 0xff);
    header.contentLengthB1  = (unsigned char) ((contentLength >> 8) & 0xff);
    header.contentLengthB0  = (unsigned char) ((contentLength     ) & 0xff);
    header.paddingLength    = (unsigned char) paddingLength;
    header.reserved         =  0;
    return header;
}
//static unsigned char* FCGI_BuildStdinBody(int nRequestId, const char *name,int nameLen, int * pOutSize, int * pOutPaddingSize)
//{
//	unsigned char* lpszBuffer = new unsigned char[FCGI_HEADER_LEN+nameLen+8];
//	int nPaddingLength = nameLen%8;
//	if (nPaddingLength>0)
//		nPaddingLength = 8-nPaddingLength;
//	//nPaddingLength = 0;
//	const FCGI_Header header = MakeHeader(FCGI_STDIN,nRequestId,nameLen,nPaddingLength);
//	memcpy(lpszBuffer,&header, FCGI_HEADER_LEN);
//	memcpy(lpszBuffer+FCGI_HEADER_LEN,name, nameLen);
//	if (nPaddingLength>0)
//		memset(lpszBuffer+(FCGI_HEADER_LEN+nameLen),0, nPaddingLength);
//	*pOutSize = FCGI_HEADER_LEN+nameLen+nPaddingLength;
//	*pOutPaddingSize = nPaddingLength;
//	return lpszBuffer;
//}
static unsigned char* FCGI_BuildStdinHeader(int nRequestId, unsigned char *lpszBuffer,int bodyLen, int * pOutSize)
{
	int nPaddingLength = bodyLen%8;
	if (nPaddingLength>0)
		nPaddingLength = 8-nPaddingLength;
	const FCGI_Header header = MakeHeader(FCGI_STDIN,nRequestId,bodyLen,nPaddingLength);
	memcpy(lpszBuffer,&header, FCGI_HEADER_LEN);
	if (nPaddingLength>0)
		memset(lpszBuffer+(FCGI_HEADER_LEN+bodyLen),0, nPaddingLength);
	*pOutSize = FCGI_HEADER_LEN+bodyLen+nPaddingLength;
	return lpszBuffer;
}
static unsigned char* FCGI_BuildParamsBody(int nRequestId, const char *name,int nameLen,const char *value,int valueLen, int * pOutSize, unsigned char* lpszInBuffer)
{
	unsigned char* lpszBuffer = lpszInBuffer!=NULL?lpszInBuffer:new unsigned char[FCGI_HEADER_LEN+16+nameLen+valueLen];
	int nOffset = FCGI_HEADER_LEN;
	if (nameLen < 0x80) {
		lpszBuffer[nOffset] = (unsigned char) nameLen;
		nOffset += 1;
	} else {
		lpszBuffer[nOffset]   = (unsigned char) ((nameLen >> 24) | 0x80);
		lpszBuffer[nOffset+1] = (unsigned char) (nameLen >> 16);
		lpszBuffer[nOffset+2] = (unsigned char) (nameLen >> 8);
		lpszBuffer[nOffset+3] = (unsigned char) nameLen;
		nOffset += 4;
	}
	if (valueLen < 0x80) {
		lpszBuffer[nOffset] = (unsigned char) valueLen;
		nOffset += 1;
	} else {
		lpszBuffer[nOffset]   = (unsigned char) ((valueLen >> 24) | 0x80);
		lpszBuffer[nOffset+1] = (unsigned char) (valueLen >> 16);
		lpszBuffer[nOffset+2] = (unsigned char) (valueLen >> 8);
		lpszBuffer[nOffset+3] = (unsigned char) valueLen;
		nOffset += 4;
	}
	int nPaddingLength = (nOffset+nameLen+valueLen)%8;
	if (nPaddingLength>0)
		nPaddingLength = 8-nPaddingLength;
	//printf("*** nPaddingLength=%d\n",nPaddingLength);
	const FCGI_Header header = MakeHeader(FCGI_PARAMS,nRequestId,nOffset-FCGI_HEADER_LEN+nameLen+valueLen,nPaddingLength);
	memcpy(lpszBuffer,&header, FCGI_HEADER_LEN);
	memcpy(lpszBuffer+nOffset,name, nameLen);
	memcpy(lpszBuffer+(nOffset+nameLen),value, valueLen);
	if (nPaddingLength>0)
		memset(lpszBuffer+(nOffset+nameLen+valueLen),0, nPaddingLength);
	*pOutSize = nOffset+nameLen+valueLen+nPaddingLength;
	return lpszBuffer;
}

class CContentTypeInfo
{
public:
	typedef boost::shared_ptr<CContentTypeInfo> pointer;
	static CContentTypeInfo::pointer create(const tstring& sContentType, bool bDownload, SCRIPT_FILE_TYPE nScriptFileType = SCRIPT_FILE_TYPE_UNKNOWN)
	{
		return CContentTypeInfo::pointer(new CContentTypeInfo(sContentType, bDownload, nScriptFileType));
	}
	tstring m_sContentType;
	bool m_bDownload;
	SCRIPT_FILE_TYPE m_nScriptFileType;

	CContentTypeInfo(const tstring& sContentType, bool bDownload, SCRIPT_FILE_TYPE nScriptFileType = SCRIPT_FILE_TYPE_UNKNOWN)
		: m_sContentType(sContentType), m_bDownload(bDownload), m_nScriptFileType(nScriptFileType)
	{}
	CContentTypeInfo(void)
		: m_bDownload(false), m_nScriptFileType(SCRIPT_FILE_TYPE_UNKNOWN)
	{}
};

CLockMap<tstring,CContentTypeInfo::pointer> theContentTypeInfoList;
extern "C" bool CGC_API CGC_Module_Init(void)
{
	theFileSystemService = theServiceManager->getService("FileSystemService");
	if (theFileSystemService.get() == NULL)
	{
		CGC_LOG((cgc::LOG_ERROR, "FileSystemService Error.\n"));
		return false;
	}
	//theStringService = theServiceManager->getService("StringService");
	//if (theStringService.get() == NULL)
	//{
	//	CGC_LOG((cgc::LOG_ERROR, "StringService Error.\n"));
	//	return false;
	//}

	cgcParameterMap::pointer initParameters = theApplication->getInitParameters();

	// Load APP calls.
	tstring xmlFile(theApplication->getAppConfPath());
	xmlFile.append("/apps.xml");
	cgcValueInfo::pointer outProperty = CGC_VALUEINFO(false);
	theFileSystemService->callService("exists", CGC_VALUEINFO(xmlFile), outProperty);
	if (outProperty->getBoolean())
	{
		theApps = XMLPARSEAPPS;
		theApps->load(xmlFile);
	}

	// Load DataSource.
	xmlFile = theApplication->getAppConfPath();
	xmlFile.append("/datasources.xml");
	outProperty->setBoolean(false);
	theFileSystemService->callService("exists", CGC_VALUEINFO(xmlFile), outProperty);
	if (outProperty->getBoolean())
	{
		theDataSources = XMLPARSEDSS;
		theDataSources->load(xmlFile);
	}

	// Load virtual hosts.
	xmlFile = theApplication->getAppConfPath();
	xmlFile.append("/hosts.xml");
	theVirtualHosts.load(xmlFile);
	thePHPFastcgiInfo = theVirtualHosts.getFastcgiInfo("php");
	if (thePHPFastcgiInfo.get()!=NULL)
	{
		const std::string sThirdParth = theSystem->getServerPath() + "/thirdparty";
		cgc::replace_string(thePHPFastcgiInfo->m_sFastcgiPath,$MYCP_THIRDPARTY_PATH,sThirdParth);
		const std::string::size_type find = thePHPFastcgiInfo->m_sFastcgiPass.find(":",1);
		if (find!=std::string::npos)
		{
			thePHPFastcgiInfo->m_sFcgiPassIp = thePHPFastcgiInfo->m_sFastcgiPass.substr(0,find);
			thePHPFastcgiInfo->m_nFcgiPassPort = atoi(thePHPFastcgiInfo->m_sFastcgiPass.substr(find+1).c_str());
		}

		// *预防前面异常退出，挂住，导致网络端口被占用错误；
#ifdef WIN32
		KillAllProcess("php-cgi.exe");
#else
		char lpszBuffer[256];
		sprintf(lpszBuffer,"ps -e|grep -v 'grep'|grep php-cgi | awk '{print $1}' | xargs kill");
		system(lpszBuffer);
		sprintf(lpszBuffer,"chmod +x \"%s/php-cgi\"",thePHPFastcgiInfo->m_sFastcgiPath.c_str());
		system(lpszBuffer);
#endif
	}

	theDefaultHost = theVirtualHosts.getVirtualHost("*");
	if (theDefaultHost.get() == NULL)
	{
		CGC_LOG((cgc::LOG_ERROR, "DefaultHost Error. %s\n",xmlFile.c_str()));
		return false;
	}
	tstring sDocumentRoot(theDefaultHost->getDocumentRoot());
	cgc::replace_string(sDocumentRoot,$MYCP_CONF_PATH,theSystem->getServerPath());
	theDefaultHost->setDocumentRoot(sDocumentRoot);
	CGC_LOG((cgc::LOG_INFO, "%s\n",theDefaultHost->getDocumentRoot().c_str()));
	theDefaultHost->setPropertys(theApplication->createAttributes());
	if (theDefaultHost->getDocumentRoot().empty())
		theDefaultHost->setDocumentRoot(theApplication->getAppConfPath());
	else
	{
		tstring sDocumentRoot(theDefaultHost->getDocumentRoot());
		namespace fs = boost::filesystem;
		fs::path src_path(sDocumentRoot);
		if (!fs::exists(src_path))
		{
			tstring sDocumentRootTemp(theDefaultHost->getDocumentRoot());
			sDocumentRoot = theApplication->getAppConfPath();
			while (!sDocumentRoot.empty() && sDocumentRootTemp.size()>4 && sDocumentRootTemp.substr(0,3)=="../")	// 处理相对路径
			{
				std::string::size_type find = sDocumentRoot.rfind("/HttpServer");
				if (find==std::string::npos)
					find = sDocumentRoot.rfind("\\HttpServer");
				if (find==std::string::npos)
					break;
				if (find==0)
					sDocumentRoot = "";
				else
					sDocumentRoot = sDocumentRoot.substr(0,find);
				sDocumentRootTemp = sDocumentRootTemp.substr(3);
			}
			sDocumentRoot.append("/");
			sDocumentRoot.append(sDocumentRootTemp);
			CGC_LOG((cgc::LOG_INFO, "%s\n",sDocumentRoot.c_str()));

			//sDocumentRoot = theApplication->getAppConfPath();
			//sDocumentRoot.append("/");
			//sDocumentRoot.append(theDefaultHost->getDocumentRoot());
			fs::path src_path2(sDocumentRoot);
			if (!fs::exists(src_path2))
			{
				CGC_LOG((cgc::LOG_ERROR, "DocumentRoot not exist. %s\n",sDocumentRoot.c_str()));
				return false;
			}

			theDefaultHost->setDocumentRoot(sDocumentRoot);
		}
	}
	theDefaultHost->m_bBuildDocumentRoot = true;

	theContentTypeInfoList.insert("ai",CContentTypeInfo::create("application/postscript",true));
	theContentTypeInfoList.insert("eps",CContentTypeInfo::create("application/postscript",true));
	theContentTypeInfoList.insert("exe",CContentTypeInfo::create("application/octet-stream",true));
	theContentTypeInfoList.insert("hlp",CContentTypeInfo::create("application/mshelp",true));
	theContentTypeInfoList.insert("chm",CContentTypeInfo::create("application/mshelp",true));
	theContentTypeInfoList.insert("doc",CContentTypeInfo::create("application/msword",true));	// application/vnd.ms-word
	theContentTypeInfoList.insert("dotx",CContentTypeInfo::create("application/vnd.openxmlformats-officedocument.wordprocessingml.template",true));
	theContentTypeInfoList.insert("docm",CContentTypeInfo::create("application/vnd.openxmlformats-officedocument.wordprocessingml.document",true));
	theContentTypeInfoList.insert("docx",CContentTypeInfo::create("application/vnd.openxmlformats-officedocument.wordprocessingml.document",true));
	theContentTypeInfoList.insert("odt",CContentTypeInfo::create("application/vnd.oasis.opendocument.text",true));
	theContentTypeInfoList.insert("xls",CContentTypeInfo::create("application/vnd.ms-excel",true));
	theContentTypeInfoList.insert("xla",CContentTypeInfo::create("application/vnd.ms-excel",true));
	theContentTypeInfoList.insert("xltx",CContentTypeInfo::create("application/vnd.openxmlformats-officedocument.spreadsheetml.template",true));
	theContentTypeInfoList.insert("xlsm",CContentTypeInfo::create("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",true));
	theContentTypeInfoList.insert("xlsx",CContentTypeInfo::create("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",true));
	theContentTypeInfoList.insert("ppt",CContentTypeInfo::create("application/vnd.ms-powerpoint",true));
	theContentTypeInfoList.insert("potx",CContentTypeInfo::create("application/vnd.openxmlformats-officedocument.presentationml.template",true));
	theContentTypeInfoList.insert("pptm",CContentTypeInfo::create("application/vnd.openxmlformats-officedocument.presentationml.presentation",true));
	theContentTypeInfoList.insert("ppsx",CContentTypeInfo::create("application/vnd.openxmlformats-officedocument.presentationml.slideshow",true));
	theContentTypeInfoList.insert("pptx",CContentTypeInfo::create("application/vnd.openxmlformats-officedocument.presentationml.presentation",true));
	theContentTypeInfoList.insert("pps",CContentTypeInfo::create("application/vnd.ms-powerpoint",true));
	theContentTypeInfoList.insert("pdf",CContentTypeInfo::create("application/pdf",false));
	theContentTypeInfoList.insert("xml",CContentTypeInfo::create("application/xml",false));
	theContentTypeInfoList.insert("swf",CContentTypeInfo::create("application/x-shockwave-flash",false));
	theContentTypeInfoList.insert("cab",CContentTypeInfo::create("application/x-shockwave-flash",false));
	// archives
	theContentTypeInfoList.insert("gz",CContentTypeInfo::create("application/x-gzip",true));
	theContentTypeInfoList.insert("tgz",CContentTypeInfo::create("application/x-gzip",true));
	theContentTypeInfoList.insert("bz",CContentTypeInfo::create("application/x-bzip2",true));
	theContentTypeInfoList.insert("bz",CContentTypeInfo::create("application/x-bzip2",true));
	theContentTypeInfoList.insert("tbz",CContentTypeInfo::create("application/x-bzip2",true));
	theContentTypeInfoList.insert("zip",CContentTypeInfo::create("application/zip",true));
	theContentTypeInfoList.insert("rar",CContentTypeInfo::create("application/x-rar",true));
	theContentTypeInfoList.insert("tar",CContentTypeInfo::create("application/x-tar",true));
	theContentTypeInfoList.insert("7z",CContentTypeInfo::create("application/x-7z-compressed",true));
	// texts
	theContentTypeInfoList.insert("csp",CContentTypeInfo::create("text/csp",false,SCRIPT_FILE_TYPE_CSP));
	theContentTypeInfoList.insert("txt",CContentTypeInfo::create("text/plain",false));
	theContentTypeInfoList.insert("php",CContentTypeInfo::create("text/x-php",false,SCRIPT_FILE_TYPE_PHP));
	theContentTypeInfoList.insert("shtml",CContentTypeInfo::create("text/html",false));
	theContentTypeInfoList.insert("html",CContentTypeInfo::create("text/html",false));
	theContentTypeInfoList.insert("htm",CContentTypeInfo::create("text/html",false));
	theContentTypeInfoList.insert("js",CContentTypeInfo::create("text/javascript",false));
	theContentTypeInfoList.insert("css",CContentTypeInfo::create("text/css",false));
	theContentTypeInfoList.insert("rtf",CContentTypeInfo::create("text/rtf",true));
	theContentTypeInfoList.insert("rtfd",CContentTypeInfo::create("text/rtfd",true));
	theContentTypeInfoList.insert("py",CContentTypeInfo::create("text/x-python",false));
	theContentTypeInfoList.insert("java",CContentTypeInfo::create("text/x-java-source",false));
	theContentTypeInfoList.insert("rb",CContentTypeInfo::create("text/x-ruby",false));
	theContentTypeInfoList.insert("sh",CContentTypeInfo::create("text/x-shellscript",false));
	theContentTypeInfoList.insert("pl",CContentTypeInfo::create("text/x-perl",false));
	theContentTypeInfoList.insert("sql",CContentTypeInfo::create("text/x-sql",false));
	// images
	theContentTypeInfoList.insert("bmp",CContentTypeInfo::create("image/x-ms-bmp",false));
	theContentTypeInfoList.insert("jpg",CContentTypeInfo::create("image/jpeg",false));
	theContentTypeInfoList.insert("jpeg",CContentTypeInfo::create("image/jpeg",false));
	theContentTypeInfoList.insert("gif",CContentTypeInfo::create("image/gif",false));
	theContentTypeInfoList.insert("png",CContentTypeInfo::create("image/png",false));
	theContentTypeInfoList.insert("tif",CContentTypeInfo::create("image/tiff",false));
	theContentTypeInfoList.insert("tiff",CContentTypeInfo::create("image/tiff",false));
	theContentTypeInfoList.insert("tga",CContentTypeInfo::create("image/x-targa",true));
	//audio
	theContentTypeInfoList.insert("mp3",CContentTypeInfo::create("audio/mpeg",false));
	theContentTypeInfoList.insert("mid",CContentTypeInfo::create("audio/midi",false));
	theContentTypeInfoList.insert("ogg",CContentTypeInfo::create("audio/ogg",false));
	theContentTypeInfoList.insert("mp4a",CContentTypeInfo::create("audio/mp4",false));
	theContentTypeInfoList.insert("wav",CContentTypeInfo::create("audio/wav",false));
	theContentTypeInfoList.insert("wma",CContentTypeInfo::create("audio/x-ms-wma",false));
	// video
	theContentTypeInfoList.insert("avi",CContentTypeInfo::create("video/x-msvideo",true));
	theContentTypeInfoList.insert("dv",CContentTypeInfo::create("video/x-dv",true));
	theContentTypeInfoList.insert("mp4",CContentTypeInfo::create("video/mp4",true));
	theContentTypeInfoList.insert("mpeg",CContentTypeInfo::create("video/mpeg",true));
	theContentTypeInfoList.insert("mpg",CContentTypeInfo::create("video/mpeg",true));
	theContentTypeInfoList.insert("mov",CContentTypeInfo::create("video/quicktime",true));
	theContentTypeInfoList.insert("wm",CContentTypeInfo::create("video/x-ms-wmv",true));
	theContentTypeInfoList.insert("flv",CContentTypeInfo::create("video/x-flv",true));
	theContentTypeInfoList.insert("mkv",CContentTypeInfo::create("video/x-matroska",true));

	//theFastcgiPHPServer = initParameters->getParameterValue("fast_cgi_php_server");
	theEnableDataSource = initParameters->getParameterValue("enable-datasource", 0)==1?true:false;
#ifdef USES_BODB_SCRIPT
	if (theEnableDataSource)
	{
		tstring cdbcDataSource = initParameters->getParameterValue("CDBCDataSource", "ds_httpserver");
		theCdbcService = theServiceManager->getCDBDService(cdbcDataSource);
		if (theCdbcService.get() == NULL)
		{
			CGC_LOG((cgc::LOG_ERROR, "HttpServer DataSource Error.\n"));
			return false;
		}
		// 
		char selectSql[512];
		int cdbcCookie = 0;
		theCdbcService->select("SELECT filename,filesize,lasttime FROM cspfileinfo_t", cdbcCookie);
		//printf("**** ret=%d,cookie=%d\n",ret,cdbcCookie);
		cgcValueInfo::pointer record = theCdbcService->first(cdbcCookie);
		while (record.get() != NULL)
		{
			//assert (record->getType() == cgcValueInfo::TYPE_VECTOR);
			//assert (record->size() == 3);

			tstring sfilename(record->getVector()[0]->getStr());
			sprintf(selectSql, "SELECT code FROM scriptitem_t WHERE filename='%s' AND length(code)=3 LIMIT 1", sfilename.c_str());
			if (theCdbcService->select(selectSql)==0)
			{
				theCdbcService->escape_string_out(sfilename);
				cgcValueInfo::pointer var_filesize = record->getVector()[1];
				cgcValueInfo::pointer var_lasttime = record->getVector()[2];
				CCSPFileInfo::pointer fileInfo = CSP_FILEINFO(sfilename, var_filesize->getIntValue(), var_lasttime->getBigIntValue());
				theFileInfos.insert(fileInfo->getFileName(), fileInfo, false);
			}else
			{
				sprintf(selectSql, "DELETE FROM scriptitem_t WHERE filename='%s'", sfilename.c_str());
				theCdbcService->execute(selectSql);
				sprintf(selectSql, "DELETE FROM cspfileinfo_t WHERE filename='%s')",sfilename.c_str());
				theCdbcService->execute(selectSql);
			}

			record = theCdbcService->next(cdbcCookie);
		}
		theCdbcService->reset(cdbcCookie);
	}
#endif

	theAppAttributes = theApplication->getAttributes(true);
	theTimerHandler = CHttpTimeHandler::create();
	theApplication->SetTimer(TIMERID_1_SECOND, 1*1000, theTimerHandler);	// 1秒检查一次

	return true;
}

extern "C" void CGC_API CGC_Module_Free(void)
{
	cgcAttributes::pointer attributes = theApplication->getAttributes();
	if (attributes.get() != NULL)
	{
		// C++ APP
		std::vector<cgcValueInfo::pointer> apps;
		attributes->getProperty((int)OBJECT_APP, apps);
		for (size_t i=0; i<apps.size(); i++)
		{
			cgcValueInfo::pointer var = apps[i];
			if (var->getType() == cgcValueInfo::TYPE_OBJECT && var->getInt() == (int)OBJECT_APP)
			{
				theServiceManager->resetService(CGC_OBJECT_CAST<cgcServiceInterface>(var->getObject()));
			}
		}

		// C++ CDBC
		std::vector<cgcValueInfo::pointer> cdbcs;
		attributes->getProperty((int)OBJECT_CDBC, cdbcs);
		for (size_t i=0; i<cdbcs.size(); i++)
		{
			cgcValueInfo::pointer var = cdbcs[i];
			if (var->getType() == cgcValueInfo::TYPE_OBJECT && var->getInt() == (int)OBJECT_CDBC)
			{
				theServiceManager->resetService(CGC_OBJECT_CAST<cgcServiceInterface>(var->getObject()));
			}

		}

		attributes->cleanAllPropertys();
	}
	theApplication->KillAllTimer();
	if (theAppAttributes.get()!=NULL)
	{
		theAppAttributes->clearAllAtrributes();
		theAppAttributes.reset();
	}

	theContentTypeInfoList.clear();
#ifdef USES_BODB_SCRIPT
	theServiceManager->retCDBDService(theCdbcService);
#endif
	theFileSystemService.reset();
	//theStringService.reset();
	theDefaultHost.reset();
	theVirtualHosts.clear();
	theApps.reset();
	theDataSources.reset();
	theFileScripts.clear();
	theFileInfos.clear();
	theIdleFileInfos.clear();
	//m_pFastCgiServer.reset();
	theTimerHandler.reset();
	if (thePHPFastcgiInfo.get()!=NULL)
	{
#ifdef WIN32
		KillAllProcess("php-cgi.exe");
#else
		char lpszBuffer[256];
		sprintf(lpszBuffer,"ps -e|grep -v 'grep'|grep php-cgi | awk '{print $1}' | xargs kill");
		system(lpszBuffer);
#endif
		thePHPFastcgiInfo.reset();
	}
}

/*
application/postscript                *.ai *.eps *.ps         Adobe Postscript-Dateien 
application/x-httpd-php             *.php *.phtml         PHP-Dateien 
audio/basic                             *.au *.snd             Sound-Dateien 
audio/x-midi                            *.mid *.midi             MIDI-Dateien 
audio/x-mpeg                         *.mp2             MPEG-Dateien 
image/x-windowdump             *.xwd             X-Windows Dump 
video/mpeg                          *.mpeg *.mpg *.mpe         MPEG-Dateien 
video/vnd.rn-realvideo            *.rmvb              realplay-Dateien 
video/quicktime                     *.qt *.mov             Quicktime-Dateien 
video/vnd.vivo                       *viv *.vivo             Vivo-Dateien
*/

void GetScriptFileType(const tstring& filename, tstring& outMimeType,bool& pOutImageOrBinary,SCRIPT_FILE_TYPE& pOutScriptFileType)
{
	pOutScriptFileType = SCRIPT_FILE_TYPE_UNKNOWN;
	outMimeType = "text/html";
	tstring::size_type find = filename.rfind(".");
	if (find == tstring::npos)
	{
		outMimeType = "application/octet-stream";
		return;
	}

	pOutImageOrBinary = true;
	tstring ext(filename.substr(find+1));
	std::transform(ext.begin(), ext.end(), ext.begin(), tolower);
	CContentTypeInfo::pointer pContentTypeInfo;
	if (theContentTypeInfoList.find(ext,pContentTypeInfo))
	{
		pOutScriptFileType = pContentTypeInfo->m_nScriptFileType;
		outMimeType = pContentTypeInfo->m_sContentType;
		pOutImageOrBinary = false;
	}
	//if (ext == "csp")
	//{
	//	pOutScriptFileType = SCRIPT_FILE_TYPE_CSP;
	//	pOutImageOrBinary = false;
	//	return;
	//}else if (ext == "php")
	//{
	//	pOutScriptFileType = SCRIPT_FILE_TYPE_PHP;
	//	pOutImageOrBinary = false;
	//	return;
	//}else if (ext == "gif")
	//	outMimeType = "image/gif";
	//else if(ext == "jpeg" || ext == "jpg" || ext == "jpe")
	//	outMimeType = "image/jpeg";
	//else if(ext == "htm" || ext == "html" || ext == "shtml")
	//{
	//	pOutImageOrBinary = false;
	//	outMimeType = "text/html";
	//}else if(ext == "js")
	//{
	//	pOutImageOrBinary = false;
	//	outMimeType = "text/javascript";
	//}else if(ext == "css")
	//{
	//	pOutImageOrBinary = false;
	//	outMimeType = "text/css";
	//}else if(ext == "txt")
	//{
	//	pOutImageOrBinary = false;
	//	outMimeType = "text/plain";
	//}else if(ext == "xls" || ext == "xla")
	//	outMimeType = "application/msexcel";
	//else if(ext == "hlp" || ext == "chm")
	//	outMimeType = "application/mshelp";
	////else if(ext == "ppt" || ext == "ppz" || ext == "pps" || ext == "pot")
	////	outMimeType = "application/mspowerpoint";
	//else if(ext == "ppt")
	//	outMimeType = "application/vnd.ms-powerpoint";
	//else if(ext == "potx")
	//	outMimeType = "application/vnd.openxmlformats-officedocument.presentationml.template";
	//else if(ext == "pptm")
	//	outMimeType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
	//else if(ext == "pptm")
	//	outMimeType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
	//else if(ext == "doc" || ext == "dot")
	//	outMimeType = "application/msword";
	//else if(ext == "exe")
	//	outMimeType = "application/octet-stream";
	//else if(ext == "pdf")
	//	outMimeType = "application/pdf";
	//else if(ext == "rtf")
	//	outMimeType = "application/rtf";
	//else if(ext == "zip")
	//	outMimeType = "application/zip";
	//else if(ext == "jar")
	//	outMimeType = "application/java-archive";
	//else if(ext == "swf" || ext == "cab")
	//	outMimeType = "application//x-shockwave-flash";
	//else if(ext == "mp3")
	//	outMimeType = "audio/mpeg";
	//else if(ext == "wav")
	//	outMimeType = "audio/x-wav";
	//else
	//	pOutImageOrBinary = false;

	//return false;
}
#ifdef USES_BODB_SCRIPT
void insertScriptItem(const tstring& code, const tstring & sFileNameTemp, const CScriptItem::pointer& scriptItem, int sub = 0)
{
	std::string sValue(scriptItem->getValue());
	theCdbcService->escape_string_in(sValue);
	const int sqlsize = sValue.size()+1000;
	char * sql = new char[sqlsize];

	sprintf(sql, "INSERT INTO scriptitem_t (filename,code,sub,itemtype,object1,object2,id,scopy,name,property,type,value) VALUES('%s','%s',%d,%d,%d,%d,'%s','%s','%s','%s','%s','%s')",
				 sFileNameTemp.c_str(), code.c_str(), sub,(int)scriptItem->getItemType(),(int)scriptItem->getOperateObject1(),(int)scriptItem->getOperateObject2(),
				 scriptItem->getId().c_str(), scriptItem->getScope().c_str(),scriptItem->getName().c_str(), scriptItem->getProperty().c_str(),scriptItem->getType().c_str(), sValue.c_str());
	theCdbcService->execute(sql);
	delete[] sql;

	char bufferCode[64];
	for (size_t i=0; i<scriptItem->m_subs.size(); i++)
	{
		CScriptItem::pointer subScriptItem = scriptItem->m_subs[i];
		sprintf(bufferCode, "%s1%03d", code.c_str(), i);
		insertScriptItem(bufferCode, sFileNameTemp, subScriptItem, 1);
	}
	for (size_t i=0; i<scriptItem->m_elseif.size(); i++)
	{
		CScriptItem::pointer subScriptItem = scriptItem->m_elseif[i];
		sprintf(bufferCode, "%s2%03d", code.c_str(), i);
		insertScriptItem(bufferCode, sFileNameTemp, subScriptItem, 2);
	}
	for (size_t i=0; i<scriptItem->m_else.size(); i++)
	{
		CScriptItem::pointer subScriptItem = scriptItem->m_else[i];
		sprintf(bufferCode, "%s3%03d", code.c_str(), i);
		insertScriptItem(bufferCode, sFileNameTemp, subScriptItem, 3);
	}
}
#endif

const int const_one_hour_seconds = 60*60;
const int const_one_day_seconds = 24*const_one_hour_seconds;
const int const_memory_size = 50*1024*1024;		// max 50MB
const int const_max_size = 50*1024*1024;		// max 50MB	// 50

void SetExpiresCache(const cgcHttpResponse::pointer& response,time_t tRequestTime, bool bIsImageOrBinary)
{
	const int nExpireSecond = bIsImageOrBinary?(10*const_one_day_seconds):(2*const_one_day_seconds);	// 文本文件：2天；其他图片等10天；
	const time_t tExpiresTime = tRequestTime + nExpireSecond;
	struct tm * tmExpiresTime = gmtime(&tExpiresTime);
	char lpszBuffer[64];
	strftime(lpszBuffer, 64, "%a, %d %b %Y %H:%M:%S GMT", tmExpiresTime);	// Tue, 19 Aug 2015 07:00:19 GMT
	response->setHeader("Expires",lpszBuffer);
	sprintf(lpszBuffer, "max-age=%d", nExpireSecond);
	response->setHeader("Cache-Control",lpszBuffer);
}
#ifndef min
#define min(a, b)  (((a) < (b)) ? (a) : (b)) 
#endif // min
extern "C" HTTP_STATUSCODE CGC_API doHttpServer(const cgcHttpRequest::pointer & request, const cgcHttpResponse::pointer& response)
{
	HTTP_STATUSCODE statusCode = STATUS_CODE_200;
	const tstring sIfModifiedSince(request->getHeader("if-modified-since"));	// for Last-Modified
	const tstring sIfNoneMatch(request->getHeader("if-none-match"));			// for ETag
	// If-None-Match,ETag
	//const tstring sIfRange = request->getHeader("If-Range");
	//printf("************** If-Modified-Since: %s\n",sIfModifiedSince.c_str());
	//printf("************** If-Range: %s\n",sIfRange.c_str());

	const tstring host(request->getHost());
	//printf("**** host=%s\n",host.c_str());
	CVirtualHost::pointer requestHost = theVirtualHosts.getVirtualHost(host);
	if (requestHost.get() == NULL && theVirtualHosts.getHosts().size() > 1)
	{
		tstring::size_type find = host.find(":");
		if (find != tstring::npos)
		{
			tstring address = host.substr(0, find);
			address.append(":*");
			requestHost = theVirtualHosts.getVirtualHost(address);
			if (requestHost.get() == NULL)
			{
				address = "*";
				address.append(host.substr(find));
				requestHost = theVirtualHosts.getVirtualHost(address);
			}
		}

		if (requestHost.get() != NULL)
		{
			requestHost->setPropertys(theApplication->createAttributes());
			theVirtualHosts.addVirtualHst(host, requestHost);
		}
	}
	if (requestHost.get() != NULL && !requestHost->m_bBuildDocumentRoot)
	{
		if (requestHost->getDocumentRoot().empty())
			requestHost->setDocumentRoot(theApplication->getAppConfPath());
		else
		{
			tstring sDocumentRoot(requestHost->getDocumentRoot());
			namespace fs = boost::filesystem;
			fs::path src_path(sDocumentRoot);
			if (!fs::exists(src_path))
			{
				tstring sDocumentRootTemp(requestHost->getDocumentRoot());
				sDocumentRoot = theApplication->getAppConfPath();
				while (!sDocumentRoot.empty() && sDocumentRootTemp.size()>4 && sDocumentRootTemp.substr(0,3)=="../")	// 处理相对路径
				{
					std::string::size_type find = sDocumentRoot.rfind("/HttpServer");
					if (find==std::string::npos)
						find = sDocumentRoot.rfind("\\HttpServer");
					if (find==std::string::npos)
						break;
					if (find==0)
						sDocumentRoot = "";
					else
						sDocumentRoot = sDocumentRoot.substr(0,find);
					sDocumentRootTemp = sDocumentRootTemp.substr(3);
				}
				sDocumentRoot.append("/");
				sDocumentRoot.append(sDocumentRootTemp);
				//printf("**** sDocumentRoot=%s\n",sDocumentRoot.c_str());
				//sDocumentRoot = theApplication->getAppConfPath();
				//sDocumentRoot.append("/");
				//sDocumentRoot.append(requestHost->getDocumentRoot());
				fs::path src_path2(sDocumentRoot);
				if (!fs::exists(src_path2))
				{
					requestHost.reset();
					CGC_LOG((cgc::LOG_ERROR, "DocumentRoot not exist. %s\n",sDocumentRoot.c_str()));
				}else
					requestHost->setDocumentRoot(sDocumentRoot);
			}
		}
		if (requestHost.get() != NULL)
			requestHost->m_bBuildDocumentRoot = true;
	}

	if (requestHost.get() == NULL)
		requestHost = theDefaultHost;

	tstring sFileName(request->getRequestURI());
	//printf("**** FileName=%s\n",sFileName.c_str());
	if (sFileName == "/")
		sFileName.append(requestHost->getIndex());
	else if (sFileName.substr(0,1) != "/")
		sFileName.insert(0, "/");
	tstring sMimeType;
	bool bIsImageOrBinary = false;
	SCRIPT_FILE_TYPE nScriptFileType = SCRIPT_FILE_TYPE_UNKNOWN;
	GetScriptFileType(sFileName,sMimeType,bIsImageOrBinary,nScriptFileType);

	// File not exist
	tstring sFilePath(requestHost->getDocumentRoot() + sFileName);
	//printf("**** FilePath=%s,sFileName=%s\n",sFilePath.c_str(),sFileName.c_str());
	namespace fs = boost::filesystem;
	fs::path src_path(sFilePath);
	if (!fs::exists(src_path))
	{
		if (nScriptFileType==SCRIPT_FILE_TYPE_PHP && thePHPFastcgiInfo.get()!=NULL)
		{
			sFileName = thePHPFastcgiInfo->m_sFastcgiIndex;
			sFilePath = requestHost->getDocumentRoot() + "/" + thePHPFastcgiInfo->m_sFastcgiIndex;
			src_path = fs::path(sFilePath);
		}else if (nScriptFileType==SCRIPT_FILE_TYPE_CSP)
		{
			sFileName = requestHost->getIndex();
			sFilePath = requestHost->getDocumentRoot() + "/" + requestHost->getIndex();
			src_path = fs::path(sFilePath);
			if (!fs::exists(src_path))
			{
				response->println("HTTP Status 404 - %s", sFileName.c_str());
				return STATUS_CODE_404;
			}
		}else
		{
			response->println("HTTP Status 404 - %s", sFileName.c_str());
			return STATUS_CODE_404;
		}

	}else if (fs::is_directory(src_path))
	{
		// ??
		if (nScriptFileType==SCRIPT_FILE_TYPE_PHP && thePHPFastcgiInfo.get()!=NULL)
		{
			sFilePath.append(thePHPFastcgiInfo->m_sFastcgiIndex);
			src_path = fs::path(sFilePath);
		}else
		{
			response->println("HTTP Status 404 - %s", sFileName.c_str());
			return STATUS_CODE_404;
		}
	}

	//printf(" **** doHttpServer: filename=%s\n",sFileName.c_str());
	if (nScriptFileType==SCRIPT_FILE_TYPE_CSP)
	{
		//fs::path src_path(sFilePath);
		const size_t fileSize = (size_t)fs::file_size(src_path);
		const time_t lastTime = fs::last_write_time(src_path);

		bool buildCSPFile = false;
		CCSPFileInfo::pointer fileInfo;
		if (!theFileInfos.find(sFileName, fileInfo))
		{
			if (!theIdleFileInfos.find(sFileName, fileInfo,true))
			{
				buildCSPFile = true;
				fileInfo = CCSPFileInfo::pointer(new CCSPFileInfo(sFileName, fileSize, lastTime));
#ifdef USES_BODB_SCRIPT
				if (theCdbcService.get()!=NULL)
				{
					tstring sFileNameTemp(sFileName);
					theCdbcService->escape_string_in(sFileNameTemp);
					char sql[512];
					sprintf(sql, "INSERT INTO cspfileinfo_t (filename,filesize,lasttime) VALUES('%s',%d,%lld)",sFileNameTemp.c_str(), fileSize, (cgc::bigint)lastTime);
					theCdbcService->execute(sql);
				}
#endif
			}
			theFileInfos.insert(sFileName,fileInfo,false);
		}
		if (!buildCSPFile && fileInfo->isModified(fileSize, lastTime))
		{
			buildCSPFile = true;
			fileInfo->setFileSize(fileSize);
			fileInfo->setLastTime(lastTime);

#ifdef USES_BODB_SCRIPT
			if (theCdbcService.get()!=NULL)
			{
				tstring sFileNameTemp(sFileName);
				theCdbcService->escape_string_in(sFileNameTemp);
				char sql[512];
				sprintf(sql, "UPDATE cspfileinfo_t SET filesize=%d,lasttime=%lld WHERE filename='%s'",fileSize, (cgc::bigint)lastTime, sFileNameTemp.c_str());
				theCdbcService->execute(sql);
			}
#endif
		}
		fileInfo->m_tRequestTime = time(0);	// ***记录最新时间，用于定时清空太久没用资源

		CFileScripts::pointer fileScript;
		if (!theFileScripts.find(sFileName, fileScript))
		{
			buildCSPFile = true;
			fileScript = CFileScripts::pointer(new CFileScripts(sFileName));
			theFileScripts.insert(sFileName, fileScript,false);
		}
		CMycpHttpServer::pointer httpServer = CMycpHttpServer::pointer(new CMycpHttpServer(request, response));
		httpServer->setSystem(theSystem);
		httpServer->setApplication(theApplication);
		httpServer->setFileSystemService(theFileSystemService);
		httpServer->setServiceManager(theServiceManager);
		httpServer->setVirtualHost(requestHost);
		httpServer->setServletName(sFileName.substr(1));
		httpServer->setApps(theApps);
		httpServer->setDSs(theDataSources);

		//printf("**** buildCSPFile=%d\n",(int)(buildCSPFile?1:0));
		if (buildCSPFile)
		{
			if (!fileScript->parserCSPFile(sFilePath.c_str())) return STATUS_CODE_404;

#ifdef USES_BODB_SCRIPT
			if (theCdbcService.get()!=NULL)
			{
				char sql[512];
				tstring sFileNameTemp(sFileName);
				theCdbcService->escape_string_in(sFileNameTemp);
				sprintf(sql, "DELETE FROM scriptitem_t WHERE filename='%s'", sFileNameTemp.c_str());
				theCdbcService->execute(sql);

				const std::vector<CScriptItem::pointer>& scripts = fileScript->getScripts();
				//printf("**** buildCSPFile filename=%s, size=%d\n",sFileName.c_str(),(int)scripts.size());
				char bufferCode[5];
				for (size_t i=0; i<scripts.size(); i++)
				{
					CScriptItem::pointer scriptItem = scripts[i];
					sprintf(bufferCode, "%04d", i);
					insertScriptItem(bufferCode, sFileNameTemp, scriptItem);
				}
			}
		}else if (fileScript->empty() && theCdbcService.get()!=NULL)
		{
			char selectSql[512];
			tstring sFileNameTemp(sFileName);
			theCdbcService->escape_string_in(sFileNameTemp);
			sprintf(selectSql, "SELECT code,sub,itemtype,object1,object2,id,scopy,name,property,type,value FROM scriptitem_t WHERE filename='%s' ORDER BY code", sFileNameTemp.c_str());

			int cdbcCookie = 0;
			//theCdbcService->select(selectSql, cdbcCookie);
			const cgc::bigint ret = theCdbcService->select(selectSql, cdbcCookie);
			//printf("**** %lld=%s\n",ret,selectSql);
			CLockMap<tstring, CScriptItem::pointer> codeScripts;
			cgcValueInfo::pointer record = theCdbcService->first(cdbcCookie);
			while (record.get() != NULL)
			{
				//assert (record->getType() == cgcValueInfo::TYPE_VECTOR);
				//assert (record->size() == 11);
				cgcValueInfo::pointer var_code = record->getVector()[0];
				cgcValueInfo::pointer var_sub = record->getVector()[1];
				cgcValueInfo::pointer var_itemtype = record->getVector()[2];
				cgcValueInfo::pointer var_object1 = record->getVector()[3];
				cgcValueInfo::pointer var_object2 = record->getVector()[4];
				cgcValueInfo::pointer var_id = record->getVector()[5];
				cgcValueInfo::pointer var_scope = record->getVector()[6];
				cgcValueInfo::pointer var_name = record->getVector()[7];
				cgcValueInfo::pointer var_property = record->getVector()[8];
				cgcValueInfo::pointer var_type = record->getVector()[9];
				cgcValueInfo::pointer var_value = record->getVector()[10];
				//const int nvlen = record->getVector()[11]->getIntValue();

				CScriptItem::pointer scriptItem = CScriptItem::pointer(new CScriptItem((CScriptItem::ItemType)var_itemtype->getIntValue()));
				scriptItem->setOperateObject1((CScriptItem::OperateObject)var_object1->getIntValue());
				scriptItem->setOperateObject2((CScriptItem::OperateObject)var_object2->getIntValue());
				scriptItem->setId(var_id->getStr());
				scriptItem->setScope(var_scope->getStr());
				scriptItem->setName(var_name->getStr());
				scriptItem->setProperty(var_property->getStr());
				scriptItem->setType(var_type->getStr());

				std::string sValue(var_value->getStr());
				if (!sValue.empty())
				{
					theCdbcService->escape_string_out(sValue);
					scriptItem->setValue(sValue);
				}
				const tstring scriptCode(var_code->getStr());
				const size_t nScriptCodeSize = scriptCode.size();
				const tstring parentCode(nScriptCodeSize<=4?"":scriptCode.substr(0, nScriptCodeSize-4));
				//printf("**** code=%s;parent_code=%s;value=%s\n",scriptCode.c_str(),parentCode.c_str(),sValue.c_str());
				CScriptItem::pointer parentScriptItem;
				if (!parentCode.empty() && codeScripts.find(parentCode, parentScriptItem))
				{
					const int nSub = var_sub->getIntValue();
					if (nSub == 2)
						parentScriptItem->m_elseif.push_back(scriptItem);
					else if (nSub == 3)
						parentScriptItem->m_else.push_back(scriptItem);
					else
						parentScriptItem->m_subs.push_back(scriptItem);
				}else
				{
					fileScript->addScript(scriptItem);
				}
				codeScripts.insert(scriptCode, scriptItem, false);

				record = theCdbcService->next(cdbcCookie);
			}
			theCdbcService->reset(cdbcCookie);
			if (fileScript->empty())
				fileInfo->setFileSize(0);
#endif
		}

		try
		{
			//printf("**** doIt filename=%s, size=%d\n",sFileName.c_str(),(int)fileScript->getScripts().size());
			const int ret = httpServer->doIt(fileScript);
			if (ret == -1)
				return STATUS_CODE_400;
			statusCode = response->getStatusCode();
		}catch(std::exception const &e)
		{
			theApplication->log(LOG_ERROR, _T("exception, RequestURL \'%s\', lasterror=0x%x\n"), request->getRequestURI().c_str(), GetLastError());
			theApplication->log(LOG_ERROR, _T("'%s'\n"), e.what());
			response->println("EXCEPTION: LastError=%d; %s", e.what());
			return STATUS_CODE_500;
		}catch(...)
		{
			theApplication->log(LOG_ERROR, _T("exception, RequestURL \'%s\', lasterror=0x%x\n"),request->getRequestURI().c_str(), GetLastError());
			response->println("EXCEPTION: LastError=%d", GetLastError());
			return STATUS_CODE_500;
		}
		return statusCode;
	}
	
	if (nScriptFileType==SCRIPT_FILE_TYPE_PHP)
	{
		if (thePHPFastcgiInfo.get()==NULL || thePHPFastcgiInfo->m_sFastcgiPass.empty())
		{
			response->println("PHP fastcgi setting error.");
			return STATUS_CODE_501;
		}

		CFastcgiRequestInfo::pointer pFastcgiRequestInfo = theTimerHandler->GetFastcgiRequestInfo(nScriptFileType,response);
		const int nRequestId = pFastcgiRequestInfo->GetRequestId();
		//printf("**** nRequestId=%d\n",nRequestId);
		cgc::CgcTcpClient::pointer& m_pFastCgiServer = pFastcgiRequestInfo->m_pFastCgiServer;
		if (m_pFastCgiServer.get()==NULL || m_pFastCgiServer->IsDisconnection())
		{
			m_pFastCgiServer = cgc::CgcTcpClient::create((cgc::TcpClient_Callback*)theTimerHandler.get(),nRequestId);
			if (m_pFastCgiServer->startClient(pFastcgiRequestInfo->GetFastcgiPass().c_str(),0)!=0 || m_pFastCgiServer->IsDisconnection())
			{
				bool bConnectError = true;
				const CRequestPassInfo::pointer& pRequestPassInfo = pFastcgiRequestInfo->GetRequestPassInfo();
				if (pRequestPassInfo->GetProcessId()>0)
				{
					//printf("**** restart fastcgi\n");
					pRequestPassInfo->KillProcess();
					theTimerHandler->CreateRequestPassInfoProcess(pRequestPassInfo);
					m_pFastCgiServer = cgc::CgcTcpClient::create(theTimerHandler.get(),nRequestId);
					bConnectError = (m_pFastCgiServer->startClient(pFastcgiRequestInfo->GetFastcgiPass().c_str(),0)!=0 || m_pFastCgiServer->IsDisconnection());
					//printf("**** restart fastcgi ok=%d\n",bConnectError?0:1);
				}
				if (bConnectError)
				{
					theTimerHandler->RemoveRequestInfo(pFastcgiRequestInfo);
					response->println("PHP fastcgi connect error, FastcgiPass=%s",thePHPFastcgiInfo->m_sFastcgiPass.c_str());
					return STATUS_CODE_501;
				}
			}
		}
		FCGI_BeginRequestRecord pBeginRequestRecord;
		memset(&pBeginRequestRecord,0,sizeof(pBeginRequestRecord));
		pBeginRequestRecord.header = MakeHeader(FCGI_BEGIN_REQUEST,nRequestId,FCGI_BEGIN_REQUEST_BODY_LEN, 0);
		const int nRole = FCGI_RESPONDER;
		pBeginRequestRecord.body.roleB1  = (unsigned char) ((nRole >> 8) & 0xff);
		pBeginRequestRecord.body.roleB0  = (unsigned char) ((nRole     ) & 0xff);
		m_pFastCgiServer->sendData((const unsigned char*)&pBeginRequestRecord,sizeof(pBeginRequestRecord));
		unsigned char * lpszSendBuffer = pFastcgiRequestInfo->GetBuffer();
		char lpszIntBuf[12];

		std::string sParam1;
		std::string sValue1;
		int nNameValue1Size = 0;
		{
			//printf("******** sFilePath=%s\n",sFilePath.c_str());
			FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_SCRIPT_FILENAME.c_str(),FASTCGI_PARAM_SCRIPT_FILENAME.size(),sFilePath.c_str(),sFilePath.size(),&nNameValue1Size,lpszSendBuffer);
			m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);
		}
		const tstring sHttpCookie(request->getHeader(Http_Cookie,""));
		//printf("******** sHttpCookie=%s\n",sHttpCookie.c_str());
		if (!sHttpCookie.empty())
		{
			FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_HTTP_COOKIE.c_str(),FASTCGI_PARAM_HTTP_COOKIE.size(),sHttpCookie.c_str(),sHttpCookie.size(),&nNameValue1Size,lpszSendBuffer);
			m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);
		}
		const tstring sHttpUserAgent(request->getHeader(Http_UserAgent,""));
		//printf("******** sHttpUserAgent=%s\n",sHttpUserAgent.c_str());
		if (!sHttpUserAgent.empty())
		{
			FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_HTTP_USER_AGENT.c_str(),FASTCGI_PARAM_HTTP_USER_AGENT.size(),sHttpUserAgent.c_str(),sHttpUserAgent.size(),&nNameValue1Size,lpszSendBuffer);
			m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);
		}
		{
			//printf("******** getRemoteAddr=%s\n",request->getRemoteAddr().c_str());
			CCgcAddress pAddress(request->getRemoteAddr());
			{
				sValue1 = pAddress.getip();
				FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_REMOTE_ADDR.c_str(),FASTCGI_PARAM_REMOTE_ADDR.size(),sValue1.c_str(),sValue1.size(),&nNameValue1Size,lpszSendBuffer);
				m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);
			}
			{
				sprintf(lpszIntBuf,"%d",pAddress.getport());
				FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_REMOTE_PORT.c_str(),FASTCGI_PARAM_REMOTE_PORT.size(),lpszIntBuf,strlen(lpszIntBuf),&nNameValue1Size,lpszSendBuffer);
				m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);
			}
		}

		// SERVER_ADDR
		const std::string& sServerAddr = request->getServerAddr();
		//printf("******** sServerAddr=%s\n",sServerAddr.c_str());
		FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_SERVER_ADDR.c_str(),FASTCGI_PARAM_SERVER_ADDR.size(),sServerAddr.c_str(),sServerAddr.size(),&nNameValue1Size,lpszSendBuffer);
		m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);
		// SERVER_PORT
		sprintf(lpszIntBuf,"%d",request->getServerPort());
		FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_SERVER_PORT.c_str(),FASTCGI_PARAM_SERVER_PORT.size(),lpszIntBuf,strlen(lpszIntBuf),&nNameValue1Size,lpszSendBuffer);
		m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);
		// SERVER_NAME
		sValue1 = "MYCP";
		FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_SERVER_NAME.c_str(),FASTCGI_PARAM_SERVER_NAME.size(),sValue1.c_str(),sValue1.size(),&nNameValue1Size,lpszSendBuffer);
		m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);
		// SERVER_SOFTWARE
		sValue1 = "MYCP/2.1";
		FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_SERVER_SOFTWARE.c_str(),FASTCGI_PARAM_SERVER_SOFTWARE.size(),sValue1.c_str(),sValue1.size(),&nNameValue1Size,lpszSendBuffer);
		m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);
		// SERVER_PROTOCOL
		const tstring& sHttpVersion = request->getHttpVersion();
		//printf("******** sHttpVersion=%s\n",sHttpVersion.c_str());
		if (!sHttpVersion.empty())
		{
			FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_SERVER_PROTOCOL.c_str(),FASTCGI_PARAM_SERVER_PROTOCOL.size(),sHttpVersion.c_str(),sHttpVersion.size(),&nNameValue1Size,lpszSendBuffer);
			m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);
		}
		// REQUEST_URI
		//const tstring& sRequestURI = request->getRequestURI();
		////printf("******** sRequestURI=%s\n",sRequestURI.c_str());
		FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_REQUEST_URI.c_str(),FASTCGI_PARAM_REQUEST_URI.size(),sFileName.c_str(),sFileName.size(),&nNameValue1Size,lpszSendBuffer);
		m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);
		// QUERY_STRING
		const tstring& sQueryString = request->getQueryString();
		if (!sQueryString.empty())
		{
			//printf("******** sQueryString=%s\n",sQueryString.c_str());
			unsigned char* pBufferTemp = (sQueryString.size()+FCGI_HEADER_LEN+16)>DEFAULT_SEND_BUFFER_SIZE?NULL:lpszSendBuffer;
			pBufferTemp = FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_QUERY_STRING.c_str(),FASTCGI_PARAM_QUERY_STRING.size(),sQueryString.c_str(),sQueryString.size(),&nNameValue1Size,pBufferTemp);
			m_pFastCgiServer->sendData((const unsigned char*)pBufferTemp,nNameValue1Size);
			if (pBufferTemp!=lpszSendBuffer)
				delete[] pBufferTemp;
		}
		// REQUEST_METHOD
		bool bAlreadySetContentType = false;
		if (request->getHttpMethod()==HTTP_GET)
		{
			sValue1 = "GET";
			FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_REQUEST_METHOD.c_str(),FASTCGI_PARAM_REQUEST_METHOD.size(),sValue1.c_str(),sValue1.size(),&nNameValue1Size,lpszSendBuffer);
			m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);
		}else if (request->getHttpMethod()==HTTP_POST)
		{
			sValue1 = "POST";
			FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_REQUEST_METHOD.c_str(),FASTCGI_PARAM_REQUEST_METHOD.size(),sValue1.c_str(),sValue1.size(),&nNameValue1Size,lpszSendBuffer);
			m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);
			//const int nContentLength = request->getContentLength();
			//printf("**** POST CONTENT_LENGTH=%d\n",nContentLength);
			//if (nContentLength>0)
			//{
			//	sParam1 = ("CONTENT_LENGTH");
			//	char lpszBuf[12];
			//	sprintf(lpszBuf,"%d",nContentLength);
			//	unsigned char * lpBuffer = FCGI_BuildParamsBody(nRequestId,sParam1.c_str(),sParam1.size(),lpszBuf,strlen(lpszBuf),&nNameValue1Size);
			//	m_pFastCgiServer->sendData((const unsigned char*)lpBuffer,nNameValue1Size);
			//	delete[] lpBuffer;
			//}
			std::vector<cgcUploadFile::pointer> outFils;
			request->getUploadFile(outFils);
			//printf("******** UploadFile size=%d\n",outFils.size());
			if (!outFils.empty())
			{
				bAlreadySetContentType = true;
				char lpszBoundary[24];
				sprintf(lpszBoundary,"------%lld%03d%03d",(cgc::bigint)time(0),rand(),nRequestId);
				std::string sContentType("multipart/form-data; boundary=");
				sContentType.append(lpszBoundary);
				// CONTENT_TYPE
				//printf("******** CONTENT_TYPE=%s\n",sContentType.c_str());
				FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_CONTENT_TYPE.c_str(),FASTCGI_PARAM_CONTENT_TYPE.size(),sContentType.c_str(),sContentType.size(),&nNameValue1Size,lpszSendBuffer);
				m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);
				int nContentLength = 0;
				std::vector<std::string> pBoundaryHeadList;
				std::vector<std::string> pBoundaryEndList;
				char lpszBuffer[512];
				for (size_t i=0;i<outFils.size();i++)
				{
					const cgcUploadFile::pointer& pUploadFile = outFils[i];
					sprintf(lpszBuffer,"--%s\r\nContent-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\nContent-Type: %s\r\n\r\n",
						lpszBoundary,pUploadFile->getName().c_str(),pUploadFile->getFileName().c_str(),pUploadFile->getContentType().c_str());
					pBoundaryHeadList.push_back(lpszBuffer);
					nContentLength += strlen(lpszBuffer);
					nContentLength += pUploadFile->getFileSize();
					if (i+1==outFils.size())
					{
						sprintf(lpszBuffer,"\r\n--%s--\r\n",lpszBoundary);
					}else
					{
						sprintf(lpszBuffer,"\r\n--%s\r\n",lpszBoundary);
					}
					pBoundaryEndList.push_back(lpszBuffer);
					nContentLength += strlen(lpszBuffer);
				}
				//Content_type:multipart/form-data; boundary=---------------------------2571883601823556076521314992  
				//Content_length:2668  
				//Standard input:  
				//-----------------------------2571883601823556076521314992  
				//Content-Disposition: form-data; name="file"; filename="nginx.conf"  
				//Content-Type: application/octet-stream  
				//
				//内容..........  
				//-----------------------------2571883601823556076521314992--
				//printf("******** CONTENT_LENGTH=%d\n",nContentLength);
				// CONTENT_LENGTH
				sprintf(lpszIntBuf,"%d",nContentLength);
				FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_CONTENT_LENGTH.c_str(),FASTCGI_PARAM_CONTENT_LENGTH.size(),lpszIntBuf,strlen(lpszIntBuf),&nNameValue1Size,lpszSendBuffer);
				m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);

				// end FCGI_PARAMS
				FCGI_Header header = MakeHeader(FCGI_PARAMS,nRequestId,0,0);
				m_pFastCgiServer->sendData((const unsigned char*)&header,FCGI_HEADER_LEN);

				const int const_send_buffer = DEFAULT_SEND_BUFFER_SIZE;
				//int nPaddingLength = 0;
				for (size_t i=0;i<outFils.size();i++)
				{
					const std::string& sBoundaryHead = pBoundaryHeadList[i];
					const std::string& sBoundaryEnd = pBoundaryEndList[i];
					const cgcUploadFile::pointer& pUploadFile = outFils[i];
					const size_t nFileSize = pUploadFile->getFileSize();
					FILE * f = fopen(pUploadFile->getFilePath().c_str(),"rb");
					if (f==NULL)
					{
						printf("******** %s, not exist\n",pUploadFile->getFilePath().c_str());
						break;
					}

					size_t nToReadSize = min(nFileSize,const_send_buffer-FCGI_HEADER_LEN-sBoundaryHead.size()-8);	// 8=paddinglength
					size_t nReadedSize = 0;
					memcpy(lpszSendBuffer+FCGI_HEADER_LEN,sBoundaryHead.c_str(),sBoundaryHead.size());
					size_t nBufferOffset = FCGI_HEADER_LEN+sBoundaryHead.size();
					while (nReadedSize<nFileSize)
					{
						const size_t nReadSize = fread(lpszSendBuffer+nBufferOffset,1,nToReadSize,f);
						if (nReadSize==0)
							break;
						nReadedSize += nReadSize;
						nBufferOffset += nReadSize;
						if (nBufferOffset+sBoundaryEnd.size()<=const_send_buffer)
						{
							memcpy(lpszSendBuffer+nBufferOffset,sBoundaryEnd.c_str(),sBoundaryEnd.size());
							nBufferOffset += sBoundaryEnd.size();
						}						
						FCGI_BuildStdinHeader(nRequestId,lpszSendBuffer,nBufferOffset-FCGI_HEADER_LEN,&nNameValue1Size);
						m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);
						nToReadSize = min(const_send_buffer-FCGI_HEADER_LEN-8, nFileSize-nReadedSize);
						nBufferOffset = FCGI_HEADER_LEN;
					}
					fclose(f);
				}
			}
		}
		// CONTENT_TYPE
		if (!bAlreadySetContentType)
		{
			const tstring& sContentType = request->getContentType();
			//printf("******** sContentType=%s\n",sContentType.c_str());
			if (!sContentType.empty())
			{
				FCGI_BuildParamsBody(nRequestId,FASTCGI_PARAM_CONTENT_TYPE.c_str(),FASTCGI_PARAM_CONTENT_TYPE.size(),sContentType.c_str(),sContentType.size(),&nNameValue1Size,lpszSendBuffer);
				m_pFastCgiServer->sendData((const unsigned char*)lpszSendBuffer,nNameValue1Size);
			}
			// end FCGI_PARAMS
			FCGI_Header header = MakeHeader(FCGI_PARAMS,nRequestId,0,0);
			m_pFastCgiServer->sendData((const unsigned char*)&header,FCGI_HEADER_LEN);
		}

		// end FCGI_STDIN
		FCGI_Header header = MakeHeader(FCGI_STDIN,nRequestId,0,0);
		m_pFastCgiServer->sendData((const unsigned char*)&header,FCGI_HEADER_LEN);
		// 
		for (int i=0;i<thePHPFastcgiInfo->m_nResponseTimeout*100;i++)	// default 30S
		{
			if (pFastcgiRequestInfo->GetResponseState()==REQUEST_INFO_STATE_FCGI_DISCONNECTED)
				return STATUS_CODE_200;
			else if (!theApplication->isInited())
				break;
#ifdef WIN32
			Sleep(10);
#else
			usleep(10000);
#endif
		}
		//header = MakeHeader(FCGI_ABORT_REQUEST,nRequestId,0,0);
		//m_pFastCgiServer->sendData((const unsigned char*)&header,FCGI_HEADER_LEN);
		CRequestPassInfo::pointer pRequestPassInfo = pFastcgiRequestInfo->GetRequestPassInfo();
		if (pRequestPassInfo->GetProcessId()>0)
		{
			pRequestPassInfo->KillProcess();
			theTimerHandler->CreateRequestPassInfoProcess(pRequestPassInfo);
			m_pFastCgiServer->stopClient();
		}
		theTimerHandler->RemoveRequestInfo(pFastcgiRequestInfo);
		return STATUS_CODE_200;
	}
	
	{
		// 返回
		//HTTP/1.0 200 OK
		//	Content-Length: 13057672
		//	Content-Type: application/octet-stream
		//	Last-Modified: Wed, 10 Oct 2005 00:56:34 GMT
		//	Accept-Ranges: bytes
		//	ETag: "2f38a6cac7cec51:160c"
		//	Server: Microsoft-IIS/6.0
		//	X-Powered-By: ASP.NET
		//	Date: Wed, 16 Nov 2005 01:57:54 GMT
		//	Connection: close 

		// Content-Range: bytes 500-999/1000 
		// Content-Range字段说明服务器返回了文件的某个范围及文件的总长度。这时Content-Length字段就不是整个文件的大小了，
		// 而是对应文件这个范围的字节数，这一点一定要注意。

		// 请求
		//Range: bytes=500-      表示读取该文件的500-999字节，共500字节。
		//Range: bytes=500-599   表示读取该文件的500-599字节，共100字节。

		CResInfo::pointer pResInfo = CGC_OBJECT_CAST<CResInfo>(theAppAttributes->getAttribute(ATTRIBUTE_FILE_INFO, sFilePath));
		if (pResInfo.get() == NULL)
		{
			pResInfo = CResInfo::create(sFilePath,sMimeType);
			theAppAttributes->setAttribute(ATTRIBUTE_FILE_INFO,sFilePath,pResInfo);
		}
		pResInfo->m_tRequestTime = time(0);	// ***记录最新时间，用于定时清空太久没用资源

		//fs::path src_path(sFilePath);
		const time_t lastTime = fs::last_write_time(src_path);
		{
			boost::mutex::scoped_lock lock(pResInfo->m_mutex);
			if (pResInfo->m_tModifyTime != lastTime)
			{
				std::fstream stream;
				stream.open(sFilePath.c_str(), std::ios::in|std::ios::binary);
				if (!stream.is_open())
				{
					return STATUS_CODE_404;
				}
				pResInfo->m_nSize = (unsigned int)fs::file_size(src_path);
				pResInfo->m_tModifyTime = lastTime;
				struct tm * tmModifyTime = gmtime(&pResInfo->m_tModifyTime);
				char lpszModifyTime[128];
				strftime(lpszModifyTime, 128, "%a, %d %b %Y %H:%M:%S GMT", tmModifyTime);
				pResInfo->m_sModifyTime = lpszModifyTime;
				//// ETag
				//sprintf(lpszModifyTime,"\"%lld\"",(cgc::bigint)pResInfo->m_tModifyTime);
				//pResInfo->m_sETag = lpszModifyTime;
				// 先处理前面内存数据
				char * lpszTemp = pResInfo->m_pData;
				pResInfo->m_pData = NULL;
				// 读文件内容到内存（只读取小于50MB，超过的直接从文件读取）
				if (pResInfo->m_nSize<=const_memory_size)
				{
					char * buffer = new char[pResInfo->m_nSize+1];
					memset(buffer, 0, pResInfo->m_nSize+1);
					stream.seekg(0, std::ios::beg);
					stream.read(buffer, pResInfo->m_nSize);
					buffer[pResInfo->m_nSize] = '\0';
					pResInfo->m_pData = buffer;

					// 计算MD5
					MD5 md5;
					md5.update((const unsigned char*)buffer, pResInfo->m_nSize);
					md5.finalize();
					const std::string sFileMd5String = (const char*)md5.hex_digest();
					sprintf(lpszModifyTime,"\"%s\"",sFileMd5String.c_str());
					pResInfo->m_sETag = lpszModifyTime;
				}
				stream.close();
				if (lpszTemp != NULL)
					delete[] lpszTemp;
			}
		}
		if (sIfModifiedSince==pResInfo->m_sModifyTime)
		{
			SetExpiresCache(response,pResInfo->m_tRequestTime,bIsImageOrBinary);
			return STATUS_CODE_304;					// 304 Not Modified
		}
		if (!sIfNoneMatch.empty() && !pResInfo->m_sETag.empty() && sIfNoneMatch.find(pResInfo->m_sETag)!=tstring::npos)
		{
			SetExpiresCache(response,pResInfo->m_tRequestTime,bIsImageOrBinary);
			return STATUS_CODE_304;					// 304 Not Modified
		}

		//printf("**** %s\n",lpszFileName);
		unsigned int nRangeFrom = request->getRangeFrom();
		unsigned int nRangeTo = request->getRangeTo();
		const tstring sRange = request->getHeader(Http_Range);
		//printf("**** %s\n",sRange.c_str());
		//printf("**** Range %d-%d\n",nRangeFrom,nRangeTo);
		if (nRangeTo == 0)
			nRangeTo = pResInfo->m_nSize-1;
		unsigned int nReadTotal = nRangeTo-nRangeFrom+1;				// 重设数据长度
		//if (nReadTotal >= 1024)
		//{
		//	statusCode = STATUS_CODE_206;
		//	nReadTotal = 1024;
		//	nRangeTo = nRangeFrom+1024;
		//}
		if (nRangeTo>=pResInfo->m_nSize)
			return STATUS_CODE_416;					// 416 Requested Range Not Satisfiable
		else if (nReadTotal>const_max_size)	// 分包下载
		{
			return STATUS_CODE_413;					// 413 Request Entity Too Large
		}else if (!sRange.empty())
		{
			statusCode = STATUS_CODE_206;				// 206 Partial Content
		}
		SetExpiresCache(response,pResInfo->m_tRequestTime,bIsImageOrBinary);
		//const int nExpireSecond = bIsImageOrBinary?(10*const_one_day_seconds):const_one_day_seconds;	// 文本文件：10分钟；其他图片等=3600=60分钟；
		//const time_t tExpiresTime = pResInfo->m_tRequestTime + nExpireSecond;
		//struct tm * tmExpiresTime = gmtime(&tExpiresTime);
		//char lpszBuffer[64];
		//strftime(lpszBuffer, 64, "%a, %d %b %Y %H:%M:%S GMT", tmExpiresTime);	// Tue, 19 Aug 2015 07:00:19 GMT
		//response->setHeader("Expires",lpszBuffer);
		//sprintf(lpszBuffer, "max-age=%d", nExpireSecond);
		//response->setHeader("Cache-Control",lpszBuffer);
		////response->setHeader("Cache-Control","max-age=8640000");	// 86400=1天；100天

		//char lpszBuffer[512];
		//sprintf(lpszBuffer,"attachment;filename=%s%s",sResourceId.c_str(),sExt.c_str());	// 附件方式下载
		//sprintf(lpszBuffer,"inline;filename=%s%s",sResourceId.c_str(),sExt.c_str());		// 网页打开
		//response->setHeader(Http_ContentDisposition,lpszBuffer);
		if (pResInfo->m_pData!=NULL && !pResInfo->m_sETag.empty())
			response->setHeader("ETag",pResInfo->m_sETag);
		response->setHeader("Last-Modified",pResInfo->m_sModifyTime);
		response->setContentType(sMimeType);
		if (statusCode == STATUS_CODE_206)
		{
			response->setHeader(Http_AcceptRanges,"bytes");
		}
//		if (statusCode == STATUS_CODE_206)
//		{
//			//HTTP/1.1 200 OK  
//			//Transfer-Encoding: chunked  
//			//...  
//			//5  
//			//Hello  
//			//6  
//			//world!  
//			//0 
//			response->setHeader("Transfer-Encoding","chunked");	// 分段传输
//
//			response->setHeader(Http_AcceptRanges,"bytes");
//			response->setHeader("ETag",sFilePath);
//			response->setHeader("Connection","Keep-Alive");
//			response->setContentType(sMimeType);
//			//response->setContentType("multipart/byteranges");
//			//response->setContentType("application/octet-stream");	// 会提示下载
//			while(!response->isInvalidate())
//			{
//				printf("**** read from memory: %d-%d/%d\n",nRangeFrom,nRangeTo,pResInfo->m_nSize);
//				sprintf(lpszBuffer,"bytes %d-%d/%d",nRangeFrom,nRangeTo,pResInfo->m_nSize);
//				response->setHeader(Http_ContentRange,lpszBuffer);
//				response->writeData(pResInfo->m_pData+nRangeFrom, nReadTotal);
//				if (nRangeFrom == 0)
//					response->sendResponse(STATUS_CODE_200);
//				else if (nRangeTo>=(pResInfo->m_nSize-1))
//				{
//					response->sendResponse(STATUS_CODE_200);
//					break;
//				}else
//					response->sendResponse(STATUS_CODE_206);
//				nRangeFrom += nReadTotal;
//				nRangeTo += nReadTotal;
//				if (nRangeTo>pResInfo->m_nSize-1)
//					nRangeTo=pResInfo->m_nSize-1;
//#ifdef WIN32
//				Sleep(100);
//#else
//				usleep(100000);
//#endif
//			}
//			return statusCode;;
//		}else
//		{
//			response->setContentType(sMimeType);
//		}
		if (nReadTotal > 0)
		{
			//printf("**** resid=%lld,file=%s\n",sResourceId,pResInfo->m_sFileName.c_str());
			if (pResInfo->m_pData == NULL)
			{
				// 读文件
				//printf("**** read from file: %d-%d/%d\n",nRangeFrom,nRangeTo,pResInfo->m_nSize);
				std::fstream stream;
				stream.open(pResInfo->m_sFileName.c_str(), std::ios::in|std::ios::binary);
				if (!stream.is_open())
				{
					return STATUS_CODE_404;
				}
				stream.seekg(nRangeFrom, std::ios::beg);
				char * buffer = new char[nReadTotal+1];
				memset(buffer, 0, nReadTotal+1);
				stream.read(buffer, nReadTotal);
				stream.clear();
				stream.close();
				buffer[nReadTotal] = '\0';
				response->writeData(buffer, nReadTotal);
				// 计算MD5
				MD5 md5;
				md5.update((const unsigned char*)buffer, nReadTotal);
				md5.finalize();
				const std::string sFileMd5String = (const char*)md5.hex_digest();
				sprintf(buffer,"\"%s\"",sFileMd5String.c_str());
				pResInfo->m_sETag = buffer;
				response->setHeader("ETag",pResInfo->m_sETag);
				delete[] buffer;
			}else
			{
				// 读内存
				//printf("**** read from memory: %d-%d/%d\n",nRangeFrom,nRangeTo,pResInfo->m_nSize);
				try
				{
					response->writeData(pResInfo->m_pData+nRangeFrom, nReadTotal);
				}catch(std::exception const &)
				{
					return STATUS_CODE_500;
				}catch(...)
				{
					return STATUS_CODE_500;
				}
			}
		}else
		{
			// ?
		}
	//	tfstream stream;
	//	stream.open(sFilePath.c_str(), std::ios::in|std::ios::binary);
	//	if (!stream.is_open())
	//	{
	//		return STATUS_CODE_404;
	//	}
	//	int nRangeFrom = request->getRangeFrom();
	//	int nRangeTo = request->getRangeTo();
	//	const tstring sRange = request->getHeader(Http_Range);
	//	//printf("**** %s\n",sRange.c_str());
	//	//printf("**** Range %d-%d\n",nRangeFrom,nRangeTo);
	//	stream.seekg(0, std::ios::end);
	//	int nTotal = stream.tellg();
	//	if (nRangeTo == 0)
	//		nRangeTo = nTotal;
	//	int nReadTotal = nRangeTo-nRangeFrom;				// 重设数据长度
	//	if (nReadTotal > const_max_size)					// 分包下载
	//	{
	//		return STATUS_CODE_413;							// 413 Request Entity Too Large
	//		nReadTotal = const_max_size;
	//		statusCode = STATUS_CODE_206;
	//	}else if (!sRange.empty())
	//	{
	//		statusCode = STATUS_CODE_206;
	//	}
	//	char lpszBuffer[512];
	//	//sprintf(lpszBuffer,"attachment;filename=%s%s",sResourceId.c_str(),sExt.c_str());	// 附件方式下载
	//	//sprintf(lpszBuffer,"inline;filename=%s%s",sResourceId.c_str(),sExt.c_str());		// 网页打开
	//	//response->setHeader(Http_ContentDisposition,lpszBuffer);
	//	if (statusCode == STATUS_CODE_206)
	//	{
	//		sprintf(lpszBuffer,"bytes %d-%d/%d",nRangeFrom,nRangeTo,nTotal);
	//		response->setHeader(Http_ContentRange,lpszBuffer);
	//	}
	//	response->setHeader("Last-Modified",lpszModifyTime);
	//	response->setHeader("Cache-Control","max-age=8640000");	// 86400=1天
	//	response->setHeader(Http_AcceptRanges,"bytes");
	//	response->setContentType(sMimeType);
	//	if (nReadTotal > 0)
	//	{
	//		stream.seekg(nRangeFrom, std::ios::beg);
	//		char * buffer = new char[nReadTotal+1];
	//		memset(buffer, 0, nReadTotal+1);
	//		stream.read(buffer, nReadTotal);
	//		buffer[nReadTotal] = '\0';
	//		response->writeData(buffer, nReadTotal);
	//		delete[] buffer;
	//	}else
	//	{
	//		// ?
	//	}
	//	//nTotal = nRangeTo-nRangeFrom;	// 重设数据长度
	//	////nTotal = std::min(2*1024,nRangeTo-nRangeFrom);	// 重设数据长度

	//	//// STATUS_CODE_100

	//	//response->setHeader("Accept-Ranges","bytes");
	//	//response->setContentType(sMimeType);
	//	//if (nTotal > 0)
	//	//{
	//	//	stream.seekg(nRangeFrom, std::ios::beg);
	//	//	char * buffer = new char[nTotal+1];
	//	//	memset(buffer, 0, nTotal+1);
	//	//	stream.read(buffer, nTotal);
	//	//	buffer[nTotal] = '\0';
	//	//	response->writeData(buffer, nTotal);
	//	//	delete[] buffer;
	//	//}else
	//	//{
	//	//	// ?
	//	//}

	//	stream.clear();
	//	stream.close();
	}

	//printf("**** return =%d\n",statusCode);
	return statusCode;
}
